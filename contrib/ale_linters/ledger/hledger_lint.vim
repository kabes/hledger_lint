call ale#Set('ledger_hledgerlint_executable', "hledger_lint")

call ale#linter#Define('ledger', {
\   'name': 'hledger_lint',
\   'executable': {b -> ale#Var(b, 'ledger_hledgerlint_executable')},
\   'command': { b -> '%e'},
\   'callback': 'ale#handlers#unix#HandleAsError',
\   'output_stream': 'both'
\})
