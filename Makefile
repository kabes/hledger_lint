# based on Makefile from https://github.com/duarteocarmo/boilerplate

.PHONY: install
## Install for production
install:
	python -m pip install --upgrade pip
	python -m pip install -e .

.PHONY: install-dev
## Install for development
install-dev: install
	python -m pip install -e ".[dev]"


.PHONY: pip-sync-dev
## Synchronize venv with pip-sync
pip-sync-dev: requirements.txt requirements-dev.txt
	pip-sync requirements-dev.txt requirements.txt


.PHONY: clean
## Delete all temporary files
clean:
	rm -rf .ipynb_checkpoints
	rm -rf **/.ipynb_checkpoints
	rm -rf .pytest_cache
	rm -rf **/.pytest_cache
	rm -rf __pycache__
	rm -rf **/__pycache__
	rm -rf build
	rm -rf dist
	rm -rf htmlcov


.PHONY: ruff
## Lint using ruff
ruff:
	ruff check --fix hledger_lint


.PHONY: pylint
pylint:
	pylint hledger_lint/

.PHONY: mypy
mypy:
	mypy hledger_lint/


.PHONY: format
## Format files using black
format:
	ruff check --select I --fix .
	ruff format .
	#black .


.PHONY: test
## Run tests
test:
	pytest --ff -x --cov=hledger_lint --cov-report=html -l


.PHONY: refurb
refurb:
	ruff check --select FURB --preview --fix hledger_lint

.PHONY: check
## Run checks (ruff + test)
check:
	ruff check hledger_lint || true
	#black --check . || true
	refurb --enable-all --python-version 3.11 hledger_lint || true
	mypy hledger_lint || true
	vulture || true
	typos . || true


requirements.txt: pyproject.toml
	pip-compile --generate-hashes --output-file=requirements.txt --allow-unsafe pyproject.toml

requirements-dev.txt: pyproject.toml
	pip-compile --generate-hashes --extra=dev --output-file=requirements-dev.txt --allow-unsafe pyproject.toml

.PHONY: pip-compile
## update requirements*txt files
pip-compile: requirements.txt requirements-dev.txt


.PHONY: build
## build packages
build: pip-compile
	hatchling build


.PHONY: pycallgraph
pycallgraph:
	pycallgraph -i 'hledger_lint.*' graphviz -f svg -o "pycallgraph-$$(date +%s).svg" -- ./hledger_lint_run.py 2021.hledger

#################################################################################
# Self Documenting Commands                                                     #
#################################################################################

.DEFAULT_GOAL := help

# Inspired by <http://marmelab.com/blog/2016/02/29/auto-documented-makefile.html>
# sed script explained:
# /^##/:
# 	* save line in hold space
# 	* purge line
# 	* Loop:
# 		* append newline + line to hold space
# 		* go to next line
# 		* if line starts with doc comment, strip comment character off and loop
# 	* remove target prerequisites
# 	* append hold space (+ newline) to line
# 	* replace newline plus comments by `---`
# 	* print line
# Separate expressions are necessary because labels cannot be delimited by
# semicolon; see <http://stackoverflow.com/a/11799865/1968>
.PHONY: help
help:
	@echo "$$(tput bold)Available commands:$$(tput sgr0)"
	@sed -n -e "/^## / { \
		h; \
		s/.*//; \
		:doc" \
		-e "H; \
		n; \
		s/^## //; \
		t doc" \
		-e "s/:.*//; \
		G; \
		s/\\n## /---/; \
		s/\\n/ /g; \
		p; \
	}" ${MAKEFILE_LIST} \
	| awk -F '---' \
		-v ncol=$$(tput cols) \
		-v indent=19 \
		-v col_on="$$(tput setaf 6)" \
		-v col_off="$$(tput sgr0)" \
	'{ \
		printf "%s%*s%s ", col_on, -indent, $$1, col_off; \
		n = split($$2, words, " "); \
		line_length = ncol - indent; \
		for (i = 1; i <= n; i++) { \
			line_length -= length(words[i]) + 1; \
			if (line_length <= 0) { \
				line_length = ncol - indent - length(words[i]) - 1; \
				printf "\n%*s ", -indent, " "; \
			} \
			printf "%s ", words[i]; \
		} \
		printf "\n"; \
	}'

# vim:ft=make
