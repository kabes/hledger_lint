#! /usr/bin/env python3
# vim:fenc=utf-8
#
# Copyright © 2020 Karol Będkowski <Karol Będkowski@kntbk>
#
# Distributed under terms of the GPLv3 license.

"""Tlogger start file."""

from contextlib import suppress

try:
    import stackprinter

    stackprinter.set_excepthook(style="color")
except ImportError:
    with suppress(ImportError):
        from rich.traceback import install

        install()

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import main

with suppress(KeyboardInterrupt, EOFError):
    main.main()
