hledger_lint ver 1.0.x
======================

Simple hledger journal file linter dedicated to use with Vim Ale plugin.

Usage
-----

Launch hledger_lint read data from given file or stdin.

Additional settings may be define in configuration file.

hledger_lint look for configuration file in:

* .hledger_lint.toml in current directory
* hledger_lint.toml in current directory.
* file pointed by `HLEDGER_LINT_CONF` environment variable
* ~/.config/hledger_lint.toml
* ~/.hledger_lint.toml

Logging level may be adjusted by `HLEDGER_LINT_LOG_LEVEL` environment
variable. Supported levels: `debug`, `info`, `warn`, `error` (default),
`critical`.


Licence
-------

Copyright (c) Karol Będkowski, 2022-2024

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

For details please see COPYING file.
