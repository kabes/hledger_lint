# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Models definition.

"""

import datetime
import typing as ty
from dataclasses import dataclass, field
from decimal import Decimal
from enum import StrEnum

from hledger_lint import conf, hledger

__all__ = [
    "Posting",
    "Transaction",
    "Price",
    "Context",
    "Account",
    "Commodity",
    "HistPrice",
    "ParseError",
    "AssertionType",
    "Assertion",
    "Payee",
    "Tag",
]


@dataclass(slots=True)
class ParseError:
    line_no: int
    line: str
    message: str


@dataclass(slots=True)
class Price:
    raw: str
    amount: Decimal
    currency: str
    # is currency is not specified - use default currency if any
    use_default_currency: bool = False

    @staticmethod
    def new(
        amount: Decimal,
        currency: str,
        use_default_currency: bool = False,
    ) -> "Price":
        return Price(
            raw=f"{amount} {currency}",
            amount=amount,
            currency=currency,
            use_default_currency=use_default_currency,
        )

    def __mul__(self, other: ty.Self | int | Decimal) -> "Price":
        if isinstance(other, Price):
            return Price.new(
                self.amount * other.amount,
                self.currency,
                self.use_default_currency,
            )

        return Price.new(
            self.amount * other,
            self.currency,
            self.use_default_currency,
        )


class AssertionType(StrEnum):
    # assertion '='
    NORMAL = "normal"
    # strong '=='
    STRONG = "strong"
    # subaccount-inclusive "=*"
    SUBACCOUNT_INCLUSIVE = "subacc_incl"
    # subaccount-inclusive strong "==*"
    SUBACCOUNT_INCLUSIVE_STRONG = "subacc_incl-strong"


@dataclass(slots=True)
class Assertion(Price):
    assertion_type: AssertionType = AssertionType.NORMAL

    @staticmethod
    def from_price(
        p: Price,
        assertion_type: AssertionType = AssertionType.NORMAL,
    ) -> "Assertion":
        return Assertion(
            raw=p.raw,
            amount=p.amount,
            currency=p.currency,
            use_default_currency=p.use_default_currency,
            assertion_type=assertion_type,
        )


@dataclass(slots=True)
class Posting:
    raw: str
    line_no: int
    account: str
    amount: Price | None = None
    unit_price: Price | None = None
    total_price: Price | None = None
    # when account is in () or []
    is_not_balancing: bool = False

    balance_assertion: Assertion | None = None

    account_type: hledger.AccountType = hledger.AccountType.UNKNOWN

    def amount_in_currency(self, currency: str) -> Price | None:
        if self.amount is None or self.amount.currency == currency:
            return self.amount

        if self.unit_price and self.unit_price.currency == currency:
            return self.unit_price * self.amount

        if self.total_price and self.total_price.currency == currency:
            if self.amount.amount < 0:
                return self.total_price * -1

            return self.total_price

        return None


@dataclass(slots=True)
class Transaction:
    raw: str
    line_no: int
    message: str
    date: datetime.date | None
    date2: datetime.date | None = None
    postings: list[Posting | ParseError] | None = None
    is_periodic: bool = False

    # is any posting has error
    postings_with_errors: bool = False

    # transactions numbers
    numbers: list[str] = field(default_factory=list)


@dataclass(slots=True)
class Commodity:
    raw: str
    line_no: int
    currency: str
    price_format: str
    default: bool = False

    decimal_mark: str | None = None
    thousands_sep: str | None = None
    decimal: Decimal | None = None


@dataclass(slots=True)
class Account:
    raw: str
    line_no: int
    account: str = ""
    account_type: hledger.AccountType = hledger.AccountType.UNKNOWN


@dataclass(slots=True)
class HistPrice:
    raw: str
    line_no: int
    date: datetime.date
    commodity: str
    price: Price


@dataclass(slots=True)
class Payee:
    raw: str
    line_no: int
    name: str


@dataclass(slots=True)
class Tag:
    raw: str
    line_no: int
    tagname: str


class TransNumberInfo(ty.NamedTuple):
    line_no: int


@dataclass
class Context:
    conf: conf.Config

    prev_hist_price: HistPrice | None = None
    prev_transaction: Transaction | None = None

    default_commodity: str = ""
    decimal_mark: str = ""

    commodities: dict[str, Commodity] = field(default_factory=dict)
    transaction_numbers: dict[str, TransNumberInfo] = field(
        default_factory=dict
    )
    payees: set[str] = field(default_factory=set)

    current_year: int = 0

    def __post_init__(self) -> None:
        self.reset()
        self.payees.update(self.conf.payees)

    def reset(self) -> None:
        """Reset context on new file."""
        self.decimal_mark = self.conf.formatting.decimal_mark
        self.transaction_numbers.clear()


class Error(ty.NamedTuple):
    code: str
    message: str

    def __str__(self) -> str:
        return f"{self.message} [{self.code}]"

    def level(self) -> str:
        return self.code[0]


class DecimalMarkDirective(ty.NamedTuple):
    mark: str


class YearDirective(ty.NamedTuple):
    year: int


def get_main_currencies(postings: list[Posting]) -> set[str]:
    """main posting currency is currency directly on amount."""
    return {
        p.amount.currency for p in postings if p.amount and p.amount.currency
    }


def check_currency_is_base(currency: str, postings: list[Posting]) -> bool:
    return all(
        p.amount_in_currency(currency) is not None
        for p in postings
        if p.amount
    )


def get_useful_base_currency(postings: list[Posting]) -> str | None:
    for curr in get_main_currencies(postings):
        if check_currency_is_base(curr, postings):
            return curr

    return None
