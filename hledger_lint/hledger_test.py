# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from . import hledger


class TestGuessAccountType:
    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            ("expense", "expense"),
            ("expense:expense1", "expense"),
            ("income:expense1", "revenue"),
        ],
    )
    def test_guess_account_type(self, test_input, expected):
        assert hledger.guess_account_type(test_input) == expected


@pytest.mark.parametrize(
    ("amount", "fmt", "expected"),
    [
        ("1.234,56 PLN", "1.000,00 PLN", True),
        ("123.234,56 PLN", "1.000,00 PLN", True),
        ("234,56 PLN", "1.000,00 PLN", True),
        ("-1.234,56 PLN", "1.000,00 PLN", True),
        ("-123.234,56 PLN", "1.000,00 PLN", True),
        ("-234,56 PLN", "1.000,00 PLN", True),
        ("1 234.56 PLN", "1 000.00 PLN", True),
        ("123 234.56 PLN", "1 000.00 PLN", True),
        ("234.56 PLN", "1,000.00 PLN", True),
        ("-1,234.56 PLN", "1,000.00 PLN", True),
        ("-123 234.56 PLN", "1 000 000.00 PLN", True),
        ("-234, PLN", "1.000, PLN", True),
        ("123.234, PLN", "1.000, PLN", True),
        ("123.234 PLN", "1.000, PLN", True),
        ("123234 PLN", "1000, PLN", True),
        ("$1.234,56", "$1.000,00", True),
        ("PLN 123.234,56", "PLN 1.000,00", True),
        ("$ -234,56", "$ -1.000,00", True),
        ("PLN-1.234,56", "PLN1.000,00", True),
        ("PLN 1 234,56", "PLN 1 000,00", True),
        ("PLN 1 234", "PLN 1 000,", True),
        ("PLN 1 234", "PLN 1 000", True),
        ('"aa bb" 1 234', '"aa bb" 1 000', True),
        ('123.23 "aa bb"', '100.00 "aa bb"', True),
        # nok
        ("PLN 1 234", "1 000 PLN", False),
        ("$ 1.234,00", "$1 000", False),
        ("$ 1.234,00", "$1 000.00", False),
        ("1 234,56 PLN", "1.000,00 PLN", False),
        ("123,234.56 PLN", "1.000,00 PLN", False),
        ("234,56 PLN", "1.000, PLN", False),
        ("-1 234,56 PLN", "1.000,00 PLN", False),
        ("-1.234.123,56 PLN", "1'000'000.00 PLN", False),
        ("1.234.123,56 PLN", "1'000'000.00 PLN", False),
        ("1,234.56 PLN", "100,00 PLN", False),
        ("1.234.56 PLN", "100,00 PLN", False),
        ('"aa bb 1 234', '"aa bb" 1 000', False),
        ('aa bb" 1 234', '"aa bb" 1 000', False),
        ('123.23 aa bb"', '100.00 "aa bb"', False),
        ('123.23 "aa bb', '100.00 "aa bb"', False),
    ],
)
def test_match_amount_format(amount, fmt, expected):
    assert hledger.is_amount_match_format(amount, fmt) == expected


@pytest.mark.parametrize(
    ("amount", "fmt", "ignore", "expected"),
    [
        ("1234,56 PLN", "1.000,00 PLN", ".", True),
        ("123.234,56 PLN", "1000,00 PLN", ".", True),
        ("234,56 PLN", "1.000,00 PLN", ".", True),
        ("12234.56 PLN", "1,000.00 PLN", ",", True),
        # nok
        ("PLN 1234", "1 000 PLN", " ", False),
        ("$ 1.234,00", "$1 000", "'", False),
        ("$ 1.234,00", "$1 000.00", ".", False),
    ],
)
def test_match_amount_format_ignore(amount, fmt, ignore, expected):
    assert hledger.is_amount_match_format(amount, fmt, ignore) == expected


@pytest.mark.parametrize(
    ("amount", "fmt", "decimal", "expected"),
    [
        ("1.234, PLN", "1.000,00 PLN", ",", True),
        ("123.234,5 PLN", "1.000,00 PLN", ",", True),
        ("234,56 PLN", "1.000,00 PLN", ",", True),
        ("-123.234,56 PLN", "1.000,000 PLN", ",", True),
        ("-234,564 PLN", "1.000,00 PLN", ",", False),
    ],
)
def test_match_amount_format_decimal(amount, fmt, decimal, expected):
    assert (
        hledger.is_amount_match_format(amount, fmt, decimal_mark=decimal)
        == expected
    )
