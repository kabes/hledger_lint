# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Main module
"""

import fileinput
import logging
import os
import sys
import typing as ty
from pathlib import Path

from hledger_lint import model, parser, rules
from hledger_lint.conf import Config, ConfigError, load_configuration
from hledger_lint.model import (
    Account,
    Commodity,
    Context,
    Error,
    HistPrice,
    ParseError,
    Payee,
    Transaction,
)

_LOG = logging.getLogger("main")


def validate_transaction(
    ctx: Context,
    trans: Transaction,
) -> ty.Iterator[tuple[int, Error]]:
    _LOG.debug("validate transaction: %r", trans)
    for posting in trans.postings or ():
        match posting:
            case ParseError():
                _LOG.debug("skip validate posting (error): %r", posting)
                yield (
                    posting.line_no,
                    Error("E17", f"parse error: {posting.message}"),
                )
                trans.postings_with_errors = True

            case _:
                _LOG.debug("validate posting: %r", posting)
                for erule in rules.POSTING_RULES:
                    for err in erule(ctx, posting, trans):
                        _LOG.debug("rule: %r, err: %r", erule, err)
                        yield posting.line_no, err
                        trans.postings_with_errors = True

    for rule in rules.TRANSACTION_RULES:
        for err in rule(ctx, trans):
            _LOG.debug("rule: %r, err: %r", rule, err)
            yield trans.line_no, err


def validate_hist_price(
    ctx: Context,
    hist_price: HistPrice,
) -> ty.Iterator[tuple[int, Error]]:
    for rule in rules.HIST_PRICE_RULES:
        for err in rule(ctx, hist_price):
            _LOG.debug("rule: %r, err: %r", rule, err)
            yield hist_price.line_no, err


def validate_context(ctx: Context) -> ty.Iterator[tuple[int, Error]]:
    for rule in rules.CONTEXT_RULES:
        for line_no, err in rule(ctx):
            _LOG.debug("rule: %r, err: %r", rule, err)
            yield line_no, err


def process_conf(ctx: Context, lines: ty.Iterable[str]) -> None:
    """Load configurations without validation (skip errors)."""
    for res in parser.parse_conf(ctx, lines):
        match res:
            case ParseError():
                _LOG.debug("parsed error: %r", res)

            case Commodity():
                _LOG.debug("add commodity: %r", res)
                ctx.commodities[res.currency] = res
                if res.default:
                    _LOG.debug("set default_commodity: %r", res)
                    ctx.default_commodity = res.currency

            case Account():
                _LOG.debug("add account: %r", res)
                ctx.conf.account_type[res.account.lower()] = res.account_type

            case HistPrice():
                ctx.prev_hist_price = res

            case Payee():
                ctx.payees.add(res.name)


def process(  # noqa:C901
    ctx: Context,
    lines: ty.Iterable[str],
) -> ty.Iterable[tuple[int, Error]]:
    for res in parser.parse(ctx, lines):
        match res:
            case ParseError():
                _LOG.debug("parse error: %r", res)
                yield (
                    res.line_no,
                    Error("E17", f"parse error: {res.message}"),
                )

            case Commodity():
                _LOG.debug("add commodity: %r", res)
                ctx.commodities[res.currency] = res
                if res.default:
                    _LOG.debug("set default_commodity: %r", res)
                    ctx.default_commodity = res.currency

            case Account():
                _LOG.debug("add account: %r", res)
                if res.account_type:
                    ctx.conf.account_type[res.account.lower()] = (
                        res.account_type
                    )

            case HistPrice():
                _LOG.debug("add hist_price: %r", res)
                yield from validate_hist_price(ctx, res)
                ctx.prev_hist_price = res

            case Transaction():
                yield from validate_transaction(ctx, res)
                ctx.prev_transaction = res

            case Payee():
                ctx.payees.add(res.name)

            case model.DecimalMarkDirective():
                ctx.decimal_mark = res.mark

            case model.YearDirective():
                ctx.current_year = res.year

    yield from validate_context(ctx)


def _convert_conf_commodities(
    conf: Config,
) -> ty.Iterable[tuple[str, Commodity]]:
    for curr, fmt in conf.commodity_format.items():
        if parsed_format := parser.prepare_commodity_format(fmt):
            comm = Commodity(
                line_no=0,
                raw="",
                price_format=fmt,
                currency=curr,
                decimal=parsed_format[0],
                decimal_mark=parsed_format[1],
            )
            yield curr, comm


def _load_conf() -> tuple[Context, Config]:
    try:
        conf = load_configuration()
    except ConfigError as err:
        print(f"Load config error: {err}", file=sys.stderr)
        sys.exit(1)

    ctx = Context(conf)
    ctx.commodities.update(_convert_conf_commodities(conf))

    for fname in conf.load_files:
        _LOG.debug("loading conf file %s", fname)
        ctx.reset()
        try:
            with Path(fname).open(encoding="UTF-8") as ifile:
                process_conf(ctx, ifile)

        except OSError as err:
            print(f"E:{fname}:0:0: {err}", file=sys.stderr)

    return ctx, conf


def main() -> None:
    logging.basicConfig(
        level=os.environ.get("HLEDGER_LINT_LOG_LEVEL", "ERROR").upper(),
        stream=sys.stderr,
    )

    ctx, conf = _load_conf()

    has_errors = False
    try:

        def process_lines(lines: ty.Iterable[str]) -> ty.Iterable[str]:
            for line in lines:
                if fileinput.isfirstline():
                    _LOG.debug("start loading file %s", fileinput.filename)
                    ctx.reset()

                yield line

        errors = process(ctx, process_lines(fileinput.input()))

        if ign_err := conf.ignored_errors:
            errors = (err for err in errors if err[1].code not in ign_err)

        for lineno, error in sorted(errors):
            fname = fileinput.filename()
            print(f"{error.level()}:{fname}:{lineno}:0: {error}")
            has_errors = True

    except OSError as err:
        print(f"E::{err}", file=sys.stderr)

    if has_errors:
        sys.exit(1)
