# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Configuration related functions.
"""

import logging
import os
import pprint
import tomllib
import typing as ty
from dataclasses import dataclass
from pathlib import Path

from hledger_lint import hledger

__all__ = ["Config", "load_configuration", "ConfigError"]

_LOG = logging.getLogger("conf")


@dataclass(init=False)
class ConfFormatting:
    decimal_mark: str = "."
    thousands_sep: str = ","
    date: str = "auto"

    def __repr__(self) -> str:
        return pprint.pformat(self.__dict__)


class Config:
    def __init__(self) -> None:
        self.load_files: list[str] = []
        self.formatting = ConfFormatting()
        self.account_currency: dict[str, str] = {}
        self.account_type: dict[str, hledger.AccountType] = {}
        self.commodity_format: dict[str, str] = {}
        self.ignore_accounts: list[str] = []
        self.ignored_errors: list[str] = []

        self.commodity_ignore_thousands_sep: bool = False
        self.commodity_strict_fractions: bool = False
        self.validate_payee: bool = False

        self.payees: list[str] = []

    def __repr__(self) -> str:
        return pprint.pformat(self.__dict__)

    def find_type_for_account(
        self, account: str
    ) -> hledger.AccountType | None:
        account = account.lower()
        while account:
            actype = self.account_type.get(account)
            if actype is not None:
                return actype

            idx = account.rfind(":")
            if idx <= 0:
                break

            account = account[:idx]

        return None

    def is_account_ignored(self, account: str) -> bool:
        for iacc in self.ignore_accounts:
            if iacc[-1] == "*":
                if account.startswith(iacc[:-1]):
                    return True

            elif account == iacc:
                return True

        return False

    def prepare_date(self) -> None:
        # make accounts lower-case
        for key in list(self.account_type.keys()):
            if (lkey := key.lower()) != key:
                self.account_type[lkey] = self.account_type[key]


class ConfigError(Exception):
    pass


def _update_conf_recursive(
    dst: ty.Any,  # noqa:ANN401
    src: dict[str, ty.Any],
) -> None:
    for key, val in src.items():
        if hasattr(dst, key):
            dst_obj = getattr(dst, key)
            match dst_obj:
                case dict():
                    dst_obj.update(val)
                case list() | tuple() | str() | int() | float():
                    setattr(dst, key, val)
                case object():
                    _update_conf_recursive(dst_obj, val)
                case _:
                    setattr(dst, key, val)
        else:
            setattr(dst, key, val)


def _find_config() -> Path | None:
    filepaths = (
        Path(".hledger_lint.toml"),
        Path("hledger_lint.toml"),
        Path(os.environ.get("HLEDGER_LINT_CONF") or ""),
        Path("~/.config/hledger_lint.toml").expanduser(),
        Path("~/.hledger_lint.toml").expanduser(),
    )

    for path in filepaths:
        if path.is_file():
            return path

    return None


def _load_conf(file: Path) -> Config | None:
    conf = Config()

    if not file.is_file():
        err = f"missing file {file}"
        raise ConfigError(err)

    try:
        with file.open("rb") as fin:
            loaded_conf = tomllib.load(fin)
            _update_conf_recursive(conf, loaded_conf)

    except Exception as err:  # pylint: disable=broad-exception-caught
        raise ConfigError(str(err)) from err

    conf.prepare_date()

    return conf


def _default_conf() -> Config:
    return Config()


def load_configuration() -> Config:
    """Find & load configuration file."""
    conf_filename = _find_config()
    if conf_filename:
        _LOG.debug("loading configuration from %s", conf_filename)
        if conf := _load_conf(conf_filename):
            return conf

    _LOG.debug("loading default configuration")
    return _default_conf()
