# Copyright © 2023-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,ANN001

"""
Test for transaction validations rules.
"""

import typing as ty
from contextlib import suppress

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from hledger_lint import conf, model, parser
from hledger_lint.rules import transaction as t_rules


def prepare_trans(
    lines: list[str],
    default_commodity: str = "",
    ctx: model.Context | None = None,
) -> tuple[model.Context, model.Transaction]:
    if ctx is None:
        cfg = conf.Config()
        cfg.formatting.decimal_mark = ","
        cfg.formatting.thousands_sep = "."
        ctx = model.Context(cfg)
        ctx.default_commodity = default_commodity

    trans = parser.parse_transaction(ctx, list(enumerate(lines)))
    assert isinstance(trans, model.Transaction)
    return ctx, trans


def _errors2code(errors: ty.Iterable[model.Error]) -> list[str]:
    return sorted(err.code for err in errors or [])


class TestRecRules:
    @pytest.mark.parametrize(
        "test_input",
        [
            [
                "2021-07-14 payee1|comment1",
                "    posting1   100 EUR",
                "    posting2",
            ],
            [
                "2021-07-14 payee1|comment1  (12312)",
                "    posting1   100 EUR",
                "    posting2",
            ],
            [
                "2021-07-14 payee1|comment1 (L) abc  (12312)",
                "    posting1   100 EUR",
                "    posting2",
            ],
        ],
    )
    def test_format_ok(self, test_input):
        ctx, trans = prepare_trans(test_input)
        errors = list(t_rules.rule_rec_format(ctx, trans))
        assert errors == []

    def test_format_invalid(self):
        ctx, trans = prepare_trans(
            [
                "2021-07-14",
                "    posting1   100 EUR",
                "    posting2",
            ],
        )
        errors = _errors2code(t_rules.rule_rec_format(ctx, trans))
        assert errors == ["E12"]

    @pytest.mark.parametrize(
        "test_input",
        [
            [
                "2021-07-14 |comment1",
                "    posting1   100 EUR",
                "    posting2",
            ],
            [
                "2021-07-14 |comment1 (1231231)",
                "    posting1   100 EUR",
                "    posting2",
            ],
        ],
    )
    def test_format_wrong_no_payee(self, test_input):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_format(ctx, trans))
        assert errors == ["E14"]

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                [
                    "2021-07-14 payee",
                    "    posting1   100 EUR",
                    "    posting2",
                ],
                [],
            ),
            (
                [
                    "2021-07-14 payee| (1231231)",
                    "    posting1   100 EUR",
                    "    posting2",
                ],
                ["E15"],
            ),
            (
                [
                    "2021-07-14 payee| ",
                    "    posting1   100 EUR",
                    "    posting2",
                ],
                ["E15"],
            ),
        ],
    )
    def test_format_wrong_no_comment(self, test_input, expected):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_format(ctx, trans))
        assert errors == expected

    @pytest.mark.parametrize(
        "test_input",
        [
            [
                "2024-01-01 payee",
                "    revenue   -100 EUR",
                "    expense   100 EUR",
            ],
            [
                "2024-01-01 payee",
                "    revenue   -100 EUR",
                "    expense",
            ],
            [
                "2024-01-01 payee",
                "    revenue",
                "    expense",
            ],
            [
                "2024-01-01 payee",
                "    revenue",
                "    expense:ex1",
                "    expense:ex2",
            ],
            [
                "2024-01-01 payee",
                "    revenue  -100 EUR",
                "    expense:ex1   50,00 EUR",
                "    expense:ex2",
            ],
            [
                "2024-01-01 payee",
                "    revenue  -100 EUR",
                "    expense:ex1   50,00 EUR",
                "    expense:ex2   50,00 EUR",
            ],
            [
                "2024-01-01 payee",
                "    revenue:r1  -100 EUR",
                "    revenue:r2  -20 EUR",
                "    expense:ex1   50,00 EUR",
                "    expense:ex2   70,00 EUR",
            ],
        ],
    )
    def test_balance_ok(self, test_input):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == []

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense   120 EUR",
                ],
                ["E17"],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue:r1   -100 EUR",
                    "    revenue:r2   -100 EUR",
                    "    expense   120 EUR",
                ],
                ["E17"],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue:r1   -100 EUR",
                    "    expense   120 EUR",
                ],
                ["E17"],
            ),
        ],
    )
    def test_balance_nok(self, test_input, expected):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == expected

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  120 EUR",
                    "    expense:e3 ",
                ],
                ["E18"],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    revenue    ",
                    "    expense:e2  20 EUR",
                ],
                ["E19"],
            ),
        ],
    )
    def test_balance_missing_amount(self, test_input, expected):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == expected

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  10 PLN @ 10 EUR",
                ],
                [],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  5 PLN @ 10 EUR",
                    "    expense:e2  50 EUR",
                ],
                [],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  50 PLN @ 10 EUR",
                ],
                ["E17"],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    revenue:r2   -500 EUR",
                    "    expense:e2  40 PLN @ 10 EUR",
                    "    expense:e2  10 PLN @ 10 EUR",
                    "    expense:e2  100 EUR",
                ],
                [],
            ),
        ],
    )
    def test_balance_unit_price(self, test_input, expected):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == expected

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  10 PLN @@ 100 EUR",
                ],
                [],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  5 PLN @@ 50 EUR",
                    "    expense:e2  50 EUR",
                ],
                [],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    expense:e2  50 PLN @@ 10 EUR",
                ],
                ["E17"],
            ),
            (
                [
                    "2024-01-01 payee",
                    "    revenue   -100 EUR",
                    "    revenue:r2   -500 EUR",
                    "    expense:e2  40 PLN @@ 400 EUR",
                    "    expense:e2  10 PLN @@ 100 EUR",
                    "    expense:e2  100 EUR",
                ],
                [],
            ),
        ],
    )
    def test_balance_total_price(self, test_input, expected):
        ctx, trans = prepare_trans(test_input)
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == expected

    def test_periodics(self):
        ctx, trans = prepare_trans(
            [
                "~ monthly",
                "    revenue   -100 EUR",
                "    expense   120 EUR",
            ],
        )
        errors = _errors2code(t_rules.rule_rec_balance(ctx, trans))
        assert errors == ["E17"]
