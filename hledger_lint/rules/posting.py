# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# ruff: noqa: ARG001

"""
Validation rules for postings.
"""

import typing as ty

from hledger_lint import hledger
from hledger_lint.model import Context, Error, Posting, Transaction

__all__ = ["POSTING_RULES"]


PostingRule = ty.Callable[[Context, Posting, Transaction], ty.Iterator[Error]]


def rule_posting_amount_invalid(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is None:
        return

    commodity = ctx.commodities.get(posting.amount.currency)
    if not commodity:
        return

    ignore = (
        commodity.thousands_sep
        if ctx.conf.commodity_ignore_thousands_sep
        else ""
    )
    strict_frac = ctx.conf.commodity_strict_fractions
    if not hledger.is_amount_match_format(
        posting.amount.raw,
        commodity.price_format,
        ignore or "",
        commodity.decimal_mark if strict_frac else None,
    ):
        yield Error(
            "W02",
            f"amount `{posting.amount.raw}` not match commodity format "
            f"`{commodity.price_format}`",
        )

    decimal_mark = commodity.decimal_mark
    if decimal_mark is None:
        decimal_mark = ctx.decimal_mark

    if not decimal_mark:
        return

    # when format is defined ad `100,` - decimal mark may not be inserted
    if decimal_mark[-1] in ",.":
        return

    if (
        decimal_mark not in posting.amount.raw
        or sum(1 for c in posting.amount.raw if c == decimal_mark) > 1
    ):
        yield Error("W01", f"suspected amount `{posting.amount.raw}`")


def rule_posting_amount_zero(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is not None and posting.amount.amount == 0:
        yield Error("E01", "zero amount")


def rule_posting_unknown_account(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.account.lower() not in ctx.conf.account_type:
        if ctx.conf.is_account_ignored(posting.account):
            return

        yield Error("E02", f"unknown account `{posting.account}`")


def rule_posting_unknown_account_type(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if ctx.conf.is_account_ignored(posting.account):
        return

    account_type = posting.account_type
    if not account_type or account_type == hledger.AccountType.UNKNOWN:
        yield Error("E03", "unknown account type")


def rule_posting_income_positive(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is None or posting.amount.amount is None:
        return

    if (
        posting.account_type == hledger.AccountType.REVENUE
        and posting.amount.amount > 0
    ):
        yield Error("E04", "income should be negative")


def rule_posting_expense_negative(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is None or posting.amount.amount is None:
        return

    if (
        posting.account_type == hledger.AccountType.EXPENSE
        and posting.amount.amount < 0
    ):
        yield Error("E05", "expense should be positive")


def rule_posting_no_currency(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is not None and not posting.amount.currency:
        yield Error("E06", "missing currency")


def rule_posting_unknown_currency(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if posting.amount is None or not posting.amount.currency:
        return

    if posting.amount.currency not in ctx.commodities:
        yield Error("E07", f"unknown currency `{posting.amount.currency}`")


def rule_posting_inv_bad_curency(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not posting.amount:
        return

    expected_currency = ctx.conf.account_currency.get(posting.account)
    if not expected_currency:
        return

    if expected_currency != posting.amount.currency:
        yield Error(
            "E08",
            f"wrong currency (exp `{expected_currency}`; "
            f"is `{posting.amount.currency}`)",
        )


def rule_posting_unit_price(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not posting.unit_price:
        return

    if posting.unit_price.amount <= 0:
        yield Error("E20", "unit price can't be negative or zero")

    if not posting.unit_price.currency:
        yield Error("E21", "missing currency for unit price")


def rule_posting_total_price(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not posting.total_price:
        return

    if posting.total_price.amount <= 0:
        yield Error("E22", "total price can't be negative or zero")

    if not posting.total_price.currency:
        yield Error("E23", "missing currency for total price")


def rule_posting_assertion(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not posting.balance_assertion:
        return

    if not posting.balance_assertion.currency:
        yield Error("E24", "missing currency for balance assertion")


def rule_posting_same_curency(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not posting.amount or not posting.amount.currency:
        return

    curr = posting.amount.currency

    if (posting.unit_price and posting.unit_price.currency == curr) or (
        posting.total_price and posting.total_price.currency == curr
    ):
        yield Error(
            "E26",
            "posting, unit and total price currency should not be the same",
        )


def rule_posting_missing_commodity(
    ctx: Context,
    posting: Posting,
    trans: Transaction,
) -> ty.Iterator[Error]:
    """If account has non-default commodity is required to provide amount
    and commodity for posting."""

    if posting.amount:
        return

    expected_currency = ctx.conf.account_currency.get(posting.account)
    if not expected_currency or expected_currency == ctx.default_commodity:
        return

    yield Error("E28", "missing amount for non default currency")


POSTING_RULES: list[PostingRule] = [
    rule_posting_amount_invalid,
    rule_posting_amount_zero,
    rule_posting_unknown_account,
    rule_posting_unknown_account_type,
    rule_posting_income_positive,
    rule_posting_expense_negative,
    rule_posting_no_currency,
    rule_posting_unknown_currency,
    rule_posting_inv_bad_curency,
    rule_posting_same_curency,
    rule_posting_unit_price,
    rule_posting_total_price,
    rule_posting_assertion,
    rule_posting_missing_commodity,
]
