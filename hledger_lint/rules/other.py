# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# ruff: noqa: ARG001

"""
Other validation rules
"""

import typing as ty

from hledger_lint.model import Context, Error, HistPrice

__all__ = ["HIST_PRICE_RULES"]


HistPriceRule = ty.Callable[[Context, HistPrice], ty.Iterator[Error]]


def rule_hist_price(ctx: Context, histprice: HistPrice) -> ty.Iterator[Error]:
    if not ctx.prev_hist_price:
        return

    if ctx.prev_hist_price.date > histprice.date:
        yield Error("E16", f"wrong date (prev: {ctx.prev_hist_price.date})")


HIST_PRICE_RULES: list[HistPriceRule] = [
    rule_hist_price,
]
