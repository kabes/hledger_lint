# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,ANN001

"""
Test for postings validations rules.
"""

import typing as ty
from contextlib import suppress
from decimal import Decimal

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)


from hledger_lint import conf, model, parser
from hledger_lint.rules import posting as p_rules


def prepare_trans(
    lines: list[str],
    default_commodity: str = "",
    ctx: model.Context | None = None,
) -> tuple[model.Context, model.Transaction]:
    if ctx is None:
        cfg = conf.Config()
        cfg.formatting.decimal_mark = ","
        cfg.formatting.thousands_sep = "."
        ctx = model.Context(cfg)
        ctx.default_commodity = default_commodity

    trans = parser.parse_transaction(ctx, list(enumerate(lines)))
    assert isinstance(trans, model.Transaction)
    return ctx, trans


def _errors2code(errors: ty.Iterable[model.Error]) -> list[str]:
    return sorted(err.code for err in errors or [])


class TestPostingRules:
    def test_revenue(self):
        ctx, trans = prepare_trans(
            [
                "2024-01-01 payee",
                "    revenue   -100 EUR",
                "    revenue:teste  100 EUR",
                "    income   100 EUR",
                "    incomes:abc   -100 EUR",
            ],
        )
        assert trans.postings
        for idx, exp in enumerate(([], ["E04"], ["E04"], [])):
            post = trans.postings[idx]
            assert isinstance(post, model.Posting)
            errors = _errors2code(
                p_rules.rule_posting_income_positive(ctx, post, trans),
            )
            assert errors == exp, f"idx: {idx} errors: {errors} exp {exp}"

    def test_expense(self):
        ctx, trans = prepare_trans(
            [
                "2024-01-01 payee",
                "    expense   100 EUR",
                "    expense:teste  -100 EUR",
                "    expenses   -100 EUR",
                "    expenses:abc   100 EUR",
            ],
        )
        assert trans.postings
        for idx, exp in enumerate(([], ["E05"], ["E05"], [])):
            post = trans.postings[idx]
            assert isinstance(post, model.Posting)
            errors = _errors2code(
                p_rules.rule_posting_expense_negative(ctx, post, trans),
            )
            assert errors == exp, f"idx: {idx} errors: {errors} exp {exp}"

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -100 PLN @ 1 EUR",
                ),
                [],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @ -10 EUR",
                ),
                ["E20"],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @ 0",
                ),
                ["E20", "E21"],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @ 10",
                ),
                ["E21"],
            ),
        ],
    )
    def test_unit_price(self, test_input, expected):
        ctx, trans = prepare_trans(list(test_input))
        assert trans.postings
        post = trans.postings[1]
        assert isinstance(post, model.Posting)
        errors = _errors2code(
            p_rules.rule_posting_unit_price(ctx, post, trans),
        )
        assert errors == expected

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -100 PLN @@ 1 EUR",
                ),
                [],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @@ -10 EUR",
                ),
                ["E22"],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @@ 0",
                ),
                ["E22", "E23"],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR @@ 10",
                ),
                ["E23"],
            ),
        ],
    )
    def test_total_price(self, test_input, expected):
        ctx, trans = prepare_trans(list(test_input))
        assert trans.postings
        post = trans.postings[1]
        assert isinstance(post, model.Posting)
        errors = _errors2code(
            p_rules.rule_posting_total_price(ctx, post, trans),
        )
        assert errors == expected

    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -100 PLN @@ 1 EUR = 100,00 PLN",
                ),
                [],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  -10 EUR = 100,00",
                ),
                ["E24"],
            ),
            (
                (
                    "2024-01-01 payee",
                    "    revenue   100 EUR",
                    "    expense:e2  = 0",
                ),
                ["E24"],
            ),
        ],
    )
    def test_assertion(self, test_input, expected):
        ctx, trans = prepare_trans(list(test_input))
        assert trans.postings
        post = trans.postings[1]
        assert isinstance(post, model.Posting)
        errors = _errors2code(
            p_rules.rule_posting_assertion(ctx, post, trans),
        )
        assert errors == expected

    def test_same_currency(self):
        head = "2024-01-01 payee"
        tdata = [
            ("    revenue   100 EUR", []),
            ("    expense:e2  -10 EUR @ -10 EUR", ["E26"]),
            ("    expense:e2  -10 EUR @@ -10 EUR", ["E26"]),
            ("    expense:e2  -10 PLN @ -10 EUR", []),
            ("    expense:e2  -10 PLN @@ -10 EUR", []),
            # default commodity is PLN
            ("    expense:e2  -10 PLN @ -10", ["E26"]),
            ("    expense:e2  -10 PLN @@ -10", ["E26"]),
            ("    expense:e2  -10 @ -10", ["E26"]),
            ("    expense:e2  -10 @@ -10", ["E26"]),
            ("    expense:e2  -10 @ -10 PLN", ["E26"]),
            ("    expense:e2  -10 @@ -10 PLN", ["E26"]),
            ("    expense:e2  -10 @ -10 EUR", []),
            ("    expense:e2  -10 @@ -10 EUR", []),
        ]

        data = [head] + [t[0] for t in tdata]
        ctx, trans = prepare_trans(data, default_commodity="PLN")
        assert trans.postings
        for idx, (raw, exp) in enumerate(tdata):
            post = trans.postings[idx]
            assert isinstance(post, model.Posting)
            errors = _errors2code(
                p_rules.rule_posting_same_curency(ctx, post, trans),
            )
            assert errors == exp, f"idx: {idx}, raw: {raw}, errors: {errors}"


class TestRulePostingAmount:
    def test_ok_commodity(self):
        cfg = conf.Config()
        ctx = model.Context(cfg)
        ctx.commodities["PLN"] = model.Commodity(
            "",
            0,
            "PLN",
            "100,00 PLN",
            decimal_mark=",",
            decimal=Decimal("100.00"),
        )
        ctx.commodities["TOS"] = model.Commodity(
            "",
            0,
            "TOS",
            "100 TOS",
            decimal_mark="",
            decimal=Decimal("100"),
        )
        ctx.commodities["RND"] = model.Commodity(
            "",
            0,
            "RND",
            "100.000.000,00 RND",
            decimal_mark=",",
            decimal=Decimal("100.00"),
        )

        test_data = [
            "2024-01-01 payee",
            "    revenue   100 EUR",
            "    expense:e1  -100,00 PLN",
            "    expense:e2  -100,00 PLN",
            "    expense:e3  -9999,99 PLN",
            "    expense:e4  -100 TOS",
            "    expense:e5  -10000 TOS",
            "    expense:e6   -1.234.567,12 RND",
            "    expense:e7   +1.234.567,12 RND",
            "    expense:e8   +123,12 RND",
        ]
        ctx, trans = prepare_trans(test_data, ctx=ctx)
        assert trans
        assert trans.postings

        for posting in trans.postings:
            assert isinstance(posting, model.Posting)
            assert (
                _errors2code(
                    p_rules.rule_posting_amount_invalid(ctx, posting, trans)
                )
                == []
            ), f"error on posting: {posting.raw}"
