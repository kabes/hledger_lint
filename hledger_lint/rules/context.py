# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Rules related to whole ledger file
"""

import itertools
import typing as ty
from contextlib import suppress

from hledger_lint.model import Context, Error

__all__ = ["CONTEXT_RULES"]

_MAX_NUMBER_DIST = 5


def rule_numbers_continuity(ctx: Context) -> ty.Iterator[tuple[int, Error]]:
    # process only numeric values
    numbers = []
    for number, number_info in ctx.transaction_numbers.items():
        with suppress(ValueError):
            numbers.append((int(number), number_info))

    if len(numbers) <= 1:
        return

    numbers.sort()

    for (pnum, _pinfo), (cnum, ninfo) in itertools.pairwise(numbers):
        diff = cnum - pnum
        # skip big gaps in numbers sequence - they are usually other
        # numbering scheme
        if diff > _MAX_NUMBER_DIST:
            continue

        if diff > 1:
            yield (
                ninfo.line_no,
                Error(
                    "W05",
                    f"previous transaction number `{cnum - 1}` not found",
                ),
            )


CONTEXT_RULES = [
    rule_numbers_continuity,
]
