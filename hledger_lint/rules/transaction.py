# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# ruff: noqa: ARG001

"""
Validation rules for transactions.
"""

import re
import typing as ty
from contextlib import suppress
from decimal import Decimal

from hledger_lint import hledger, model
from hledger_lint.model import Context, Error, Posting, Transaction

__all__ = ["TRANSACTION_RULES"]


TransactionRule = ty.Callable[[Context, Transaction], ty.Iterator[Error]]


def rule_rec_no_entries(
    ctx: Context,
    trans: Transaction,
) -> ty.Iterator[Error]:
    if not trans.postings:
        yield Error("E09", "no postings")
    elif len(trans.postings) == 1:
        yield Error("E27", "one postings only")


def rule_rec_order(ctx: Context, trans: Transaction) -> ty.Iterator[Error]:
    if (
        not ctx.prev_transaction
        or not ctx.prev_transaction.date
        or trans.date is None
    ):
        return

    if ctx.prev_transaction.date > trans.date:
        yield Error("E10", f"wrong date (prev: {ctx.prev_transaction.date})")


def rule_rec_dates(ctx: Context, trans: Transaction) -> ty.Iterator[Error]:
    if not trans.date2 or not trans.date:
        return

    if trans.date < trans.date2:
        yield Error("E11", f"wrong date2 (should be before {trans.date})")


_REC_FORMAT_RE = re.compile(
    r"^([*?!] +)?(?P<cmt>.+?)(\s+\((?P<num>[^()]+)\))?$"
)


def rule_rec_format(ctx: Context, trans: Transaction) -> ty.Iterator[Error]:
    # message should have format 'dates payee|comment [(number)]'
    # when number is given or '|' in message there must be not empty comment
    m = _REC_FORMAT_RE.match(trans.message)
    if not m:
        yield Error("E12", "invalid transaction format")
        return

    num = m.group("num")

    cmt = m.group("cmt")
    payee, sep, title = cmt.partition("|")

    payee = payee.strip()
    if payee:
        if ctx.conf.validate_payee and payee not in ctx.payees:
            yield Error("W04", f"unknown payee {payee}")
    else:
        yield Error("E14", "missing payee")

    # title is required where is | sign or transaction number
    if (sep or num) and not title.strip():
        yield Error("E15", "missing transaction title")


def _round_amount(ctx: Context, amount: Decimal, currency: str) -> Decimal:
    with suppress(KeyError):
        if com_dec := ctx.commodities[currency].decimal:
            return amount.quantize(com_dec)

    return amount


def rule_rec_balance(ctx: Context, trans: Transaction) -> ty.Iterator[Error]:
    if not trans.postings or trans.postings_with_errors:
        return

    # ship if there are errors
    if any(isinstance(p, Error) for p in trans.postings):
        return

    # there is no errors in trans.postings
    postings = ty.cast(list[Posting], trans.postings)

    # can check only when at most one price is missing
    missing_prices = sum(1 if p.amount is None else 0 for p in postings)
    if missing_prices > 1:
        return

    # check main currencies
    currency = model.get_useful_base_currency(postings)
    if not currency:
        # can't find base currency
        return

    total_amount: Decimal = sum(  # type: ignore
        ty.cast(model.Price, p.amount_in_currency(currency)).amount
        for p in postings
        if p.amount
    )

    total_amount = _round_amount(ctx, total_amount, currency)

    if missing_prices == 0:
        if not total_amount.is_zero():
            yield Error(
                "E17",
                f"transaction not balanced; total {total_amount}",
            )

        return

    if total_amount.is_zero():
        yield Error("E25", "missing amount is calculated as zero")
        return

    # missing amount is opposite
    total_amount = -total_amount

    # check type of account missing posting
    posting = next(p for p in postings if p.amount is None)
    if (
        posting.account_type == hledger.AccountType.REVENUE
        and total_amount > 0
    ):
        yield Error(
            "E19",
            f"missing amount should be negative (is {total_amount})",
        )
    elif (
        posting.account_type == hledger.AccountType.EXPENSE
        and total_amount < 0
    ):
        yield Error(
            "E18",
            f"missing amount should be positive (is {total_amount})",
        )


def rule_rec_number(ctx: Context, trans: Transaction) -> ty.Iterator[Error]:
    if not trans.numbers:
        return

    for number in trans.numbers:
        if (
            info := ctx.transaction_numbers.get(number)
        ) and info.line_no != trans.line_no:
            msg = (
                f"duplicated transaction number `{number}`; "
                f"previous in line {info.line_no}"
            )
            yield Error("W03", msg)


TRANSACTION_RULES: list[TransactionRule] = [
    rule_rec_no_entries,
    rule_rec_order,
    rule_rec_dates,
    rule_rec_format,
    rule_rec_balance,
    rule_rec_number,
]
