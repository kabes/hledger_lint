# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201

from contextlib import suppress
from decimal import Decimal

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from . import model


class TestPosting:
    def test_amount_in_currency_unit(self):
        price = model.Price("", Decimal(10), "EUR")
        p = model.Posting("", 0, "acc", price)
        assert p.amount_in_currency("PLN") is None
        assert p.amount_in_currency("EUR") == price

        price = model.Price("", Decimal(10), "EUR")
        unit_price = model.Price("", Decimal(12), "PLN")
        p = model.Posting("", 0, "acc", price, unit_price=unit_price)
        assert p.amount_in_currency("EUR") == price
        assert p.amount_in_currency("PLN") == model.Price(
            "120 PLN",
            Decimal(120),
            "PLN",
        )

        price = model.Price("", Decimal(-10), "EUR")
        unit_price = model.Price("", Decimal(12), "PLN")
        p = model.Posting("", 0, "acc", price, unit_price=unit_price)
        assert p.amount_in_currency("EUR") == price
        assert p.amount_in_currency("PLN") == model.Price(
            "-120 PLN",
            Decimal(-120),
            "PLN",
        )

    def test_amount_in_currency_total(self):
        price = model.Price("", Decimal(10), "EUR")
        total_price = model.Price("", Decimal(123), "PLN")
        p = model.Posting("", 0, "acc", price, total_price=total_price)
        assert p.amount_in_currency("EUR") == price
        assert p.amount_in_currency("PLN") == total_price

        price = model.Price("", Decimal(-10), "EUR")
        total_price = model.Price("", Decimal(123), "PLN")
        p = model.Posting("", 0, "acc", price, total_price=total_price)
        assert p.amount_in_currency("EUR") == price
        assert p.amount_in_currency("PLN") == model.Price(
            "-123 PLN",
            Decimal(-123),
            "PLN",
        )


class TestSupports:
    def test_find_main_currencies(self):
        postings = [
            model.Posting("", 0, "aa", model.Price("", Decimal(1), "EUR")),
            model.Posting("", 0, "aa", model.Price("", Decimal(-1), "EUR")),
        ]
        exp = ["EUR"]
        assert sorted(model.get_main_currencies(postings)) == exp

        postings = [
            model.Posting("", 0, "aa", model.Price("", Decimal(1), "EUR")),
            model.Posting("", 0, "aa", None),
        ]
        exp = ["EUR"]
        assert sorted(model.get_main_currencies(postings)) == exp

        postings = [
            model.Posting("", 0, "aa", model.Price("", Decimal(1), "EUR")),
            model.Posting("", 0, "aa", model.Price("", Decimal(-1), "PLN")),
        ]
        exp = ["EUR", "PLN"]
        assert sorted(model.get_main_currencies(postings)) == exp

        postings = [
            model.Posting("", 0, "aa", model.Price("", Decimal(1), "EUR")),
            model.Posting(
                "",
                0,
                "aa",
                model.Price("", Decimal(-1), "PLN"),
                total_price=model.Price("", Decimal(1), "EUR"),
            ),
        ]
        exp = ["EUR", "PLN"]
        assert sorted(model.get_main_currencies(postings)) == exp
