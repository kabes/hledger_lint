# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

from hledger_lint import model

from . import common
from .posting import parse_posting

__all__ = ["parse_transaction"]


def _try_parse_trans_numbers(message: str) -> list[str]:
    """Find transactions number in transaction message; should be
    enclosed in (..) at the end of message. Can contains couple numbers
    separated by comma."""
    if not message or message[-1] != ")":
        return []

    try:
        idx = message.rindex("(")
    except ValueError:
        return []

    if (numbers := message[idx + 1 : -1]) and (")" not in numbers):
        return list(filter(None, map(str.strip, numbers.split(","))))

    return []


def parse_transaction(
    ctx: model.Context,
    lines: list[tuple[int, str]],
) -> model.Transaction | model.ParseError:
    rlno, rline = lines[0]
    raw = rline

    rline = common.strip_comment(rline, "  ;", "#")
    if not rline:
        return model.ParseError(rlno, raw, "empty line")

    if rline.startswith("~ "):
        message = rline[2:].strip()
        tran_date, tran_date2 = None, None
    else:
        rdate, _sep, message = rline.partition(" ")
        date1, _sep, date2 = rdate.partition("=")

        try:
            tran_date = common.parse_date(ctx, date1)
        except ValueError:
            return model.ParseError(rlno, raw, f"invalid date {date1}")

        if date2:
            try:
                tran_date2 = common.parse_date(ctx, date2)
            except ValueError:
                return model.ParseError(rlno, raw, f"invalid date (2) {date2}")
        else:
            tran_date2 = None

    message = message.strip()

    return model.Transaction(
        raw=raw,
        line_no=rlno,
        message=message,
        date=tran_date,
        date2=tran_date2,
        postings=[
            parse_posting(ctx, lno, line)
            for lno, line in lines[1:]
            if line.lstrip()[0] not in (";", "*", "#")
        ],
        is_periodic=rline.startswith("~ "),
        numbers=_try_parse_trans_numbers(message),
    )
