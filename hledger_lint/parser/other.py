# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

import typing as ty

from hledger_lint import model

from . import common, errors

# min acceptable year
_MIN_YEAR: ty.Final[int] = 1970


def parse_payee(
    ctx: model.Context,  # noqa: ARG001
    line: str,
    line_no: int,
) -> model.Payee | model.ParseError:
    raw = line
    line = common.strip_comment(line, "  ;")

    prefix, _, name = line.partition(" ")
    if prefix.lower() != "payee":
        raise errors.InvalidFormatError

    # accept empty payee name
    if name == '""':
        return model.Payee(raw=raw, line_no=line_no, name="")

    if name := name.strip():
        return model.Payee(raw=raw, line_no=line_no, name=name)

    raise errors.InvalidFormatError


def parse_tag(
    ctx: model.Context,  # noqa: ARG001
    line: str,
    line_no: int,
) -> model.Tag | model.ParseError:
    prefix, _, tagname = line.partition(" ")
    if prefix.lower() == "tag" and (tagname := tagname.strip()):
        return model.Tag(raw=line, line_no=line_no, tagname=tagname)

    raise errors.InvalidFormatError


def parse_decimal_mark(content: str) -> model.DecimalMarkDirective:
    if (mark := common.strip_comment(content)) and mark in ".,":
        return model.DecimalMarkDirective(mark)

    raise errors.ParseError("invalid decimal mark")


def parse_year(content: str) -> model.YearDirective:
    if contentyear := common.strip_comment(content):
        try:
            year = int(contentyear)
        except ValueError as err:
            raise errors.ParseError("invalid year") from err

        if year >= _MIN_YEAR:
            return model.YearDirective(year)

    raise errors.ParseError("invalid year")
