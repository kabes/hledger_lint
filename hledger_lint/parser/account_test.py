# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, hledger, model
from hledger_lint.parser import account as a_parser, errors


class TestParseAccount:
    def test_account(self):
        ctx = model.Context(conf.Config())
        acc = a_parser.parse_account(ctx, "account Income:abs:dd", 23)
        assert acc.raw == "account Income:abs:dd"
        assert acc.account == "Income:abs:dd"
        assert acc.line_no == 23
        assert acc.account_type == "revenue"

        ctx = model.Context(conf.Config())
        acc = a_parser.parse_account(
            ctx,
            "account   ABC:abs dkldk:dd dflkfld",
            23,
        )
        assert acc.account == "ABC:abs dkldk:dd dflkfld"
        assert acc.account_type == hledger.AccountType.UNKNOWN

    @pytest.mark.parametrize(
        "account",
        [
            "account",
            "account  ; telele",
        ],
    )
    def test_account_failed(self, account):
        ctx = model.Context(conf.Config())
        with pytest.raises(errors.ParseError):
            a_parser.parse_account(ctx, account, 23)

    @pytest.mark.parametrize(
        ("account", "expected"),
        [
            ("account Income:abs:dd  ; type:Asset", "asset"),
            ("account Income:abs:dd  ; type:asset", "asset"),
            ("account Income:abs:dd  ; type:a", "asset"),
            ("account dd  ; type:l", "liability"),
            ("account dd  ; type:e", "equity"),
            ("account dd  ; type:r", "revenue"),
            ("account dd  ; type:x", "expense"),
            ("account dd  ; type:c", "cash"),
        ],
    )
    def test_account_type(self, account, expected):
        ctx = model.Context(conf.Config())
        acc = a_parser.parse_account(ctx, account, 23)
        assert acc.account_type == expected

    @pytest.mark.parametrize(
        ("account", "expected"),
        [
            (
                "account Expense:abs:dd  ; type:Asset",
                hledger.AccountType.ASSET,
            ),
            ("account Expense:abs:dd", hledger.AccountType.EXPENSE),
            ("account Assets:abc:dd", hledger.AccountType.UNKNOWN),
            ("account Assets:abc", hledger.AccountType.UNKNOWN),
            ("account Assets", hledger.AccountType.ASSET),
        ],
    )
    def test_account_type_ctx(self, account, expected):
        ctx = model.Context(conf.Config())
        ctx.conf.account_type = {
            "expense": hledger.AccountType.EXPENSE,
            "assets": hledger.AccountType.ASSET,
            "assets:abc": hledger.AccountType.UNKNOWN,
        }
        acc = a_parser.parse_account(ctx, account, 23)
        assert acc.account_type == expected, acc
