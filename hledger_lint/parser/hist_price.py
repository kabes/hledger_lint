# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

from hledger_lint import model

from . import common, errors
from .amount import parse_amount


def _next_token(instr: str) -> tuple[str, str]:
    if not instr:
        return "", ""

    if instr[0] == '"':
        if (end := instr.find('"', 2)) > 0:
            return instr[1:end], instr[end + 1 :].strip()

        raise ValueError('missing ending `"` character')

    prefix, _, rest = instr.partition(" ")
    return prefix, rest.strip()


def parse_hist_price(
    ctx: model.Context,
    line: str,
    line_no: int,
) -> model.HistPrice | model.ParseError:
    # P 2000-01-01 DOS 100,00 PLN

    parts = line.strip().split(" ", 2)
    if len(parts) < 3 or parts[0] not in ("P", "p"):  # noqa: PLR2004
        raise errors.InvalidFormatError

    try:
        histp_date = common.parse_date(ctx, parts[1])
    except ValueError as err:
        raise errors.ParseError("invalid date") from err

    try:
        histp_commodity, price = _next_token(parts[2])
    except ValueError as err:
        raise errors.ParseError(str(err)) from err

    if not histp_commodity or histp_commodity == "":
        raise errors.ParseError("missing commodity")

    if not price or price == '""':
        raise errors.ParseError("missing price")

    try:
        histp_price = parse_amount(ctx, price)
    except ValueError as err:
        errmsg = f"parse amount `{price}` error: {err}"
        raise errors.ParseError(errmsg) from err

    return model.HistPrice(
        raw=line,
        line_no=line_no,
        date=histp_date,
        commodity=histp_commodity,
        price=histp_price,
    )
