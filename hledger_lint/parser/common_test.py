# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from datetime import date

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model
from hledger_lint.parser import common


class TestParseDate:
    def test_parse_date_std(self):
        ctx = model.Context(conf.Config())
        ctx.conf.formatting.date = "%Y-%m-%d"

        assert common.parse_date(ctx, "2024-01-02") == date(2024, 1, 2)

        with pytest.raises(ValueError, match="parse date 2024-13-01 error"):
            common.parse_date(ctx, "2024-13-01")

    @pytest.mark.parametrize(
        ("test_data", "expected"),
        [
            ("2024-10-21", date(2024, 10, 21)),
            ("2024/10/21", date(2024, 10, 21)),
            ("24-10-21", date(2024, 10, 21)),
            ("24/10/21", date(2024, 10, 21)),
            ("2024.10.21", date(2024, 10, 21)),
            ("24.10.21", date(2024, 10, 21)),
            ("10/21", date.today().replace(month=10, day=21)),
            ("10-21", date.today().replace(month=10, day=21)),
            ("10.21", date.today().replace(month=10, day=21)),
        ],
    )
    def test_parse_date_auto(self, test_data, expected):
        ctx = model.Context(conf.Config())
        ctx.conf.formatting.date = "auto"

        assert common.parse_date(ctx, test_data) == expected

    def test_parse_date_year_ctx(self):
        ctx = model.Context(conf.Config())
        ctx.conf.formatting.date = "auto"
        ctx.current_year = 0

        assert common.parse_date(ctx, "10/21") == date.today().replace(
            month=10, day=21
        )

        ctx.current_year = 2020
        assert common.parse_date(ctx, "10/21") == date(2020, 10, 21)

    def test_parse_date_fail(self):
        ctx = model.Context(conf.Config())
        ctx.conf.formatting.date = "auto"

        with pytest.raises(ValueError, match="parse date 13/10 error"):
            assert common.parse_date(ctx, "13/10")
