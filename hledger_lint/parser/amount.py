# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

from decimal import Decimal, InvalidOperation

from hledger_lint import model

from . import errors


def parse_float(
    ctx: model.Context,
    inp: str,
    decimal_mark: str | None = None,
) -> Decimal | None:
    if decimal_mark is None:
        decimal_mark = ctx.decimal_mark

    # remove special and white characters without decimal_mark
    inp = inp.replace(" ", "").replace("_", "").replace("'", "")
    if not inp:
        return None

    match decimal_mark:
        case ".":
            inp = inp.replace(",", "")
        case ",":
            inp = inp.replace(".", "").replace(",", ".")

    return Decimal(inp)


_FORBIDDEN_COMMODDITY_CHARS = ",..@!~`;':{}[]#%^&*()-=_+\\|/?<>"


def _validate_currency(currency: str) -> str | None:  # noqa:PLR0911
    if currency == '""':
        return None

    if currency == '"':
        return "too short"

    if currency[0] == '"':
        if currency[-1] != '"':
            return 'missing or wrong closing `"`'

        currency = currency[1:-1]

    elif " " in currency:
        return "spaces in unquoted currency"

    if not currency.strip():
        return "too short"

    if '"' in currency:
        return 'currency has inner `"`'

    # check forbidden characters in commodity
    if any(c in currency for c in _FORBIDDEN_COMMODDITY_CHARS):
        return "invalid char in commodity"

    return None


def _parse_amount_split_parts(inp: str) -> tuple[str, str]:
    """Split price into (amount, currency)"""

    if inp[0] == '"':
        idx = inp.find('"', 2)
        if idx < 0:
            err = 'missing closing `"`'
            raise ValueError(err)

        val_part, curr_part = inp[idx + 1 :].strip(), inp[: idx + 1].strip()

    # currency starts with digit
    elif inp[0] in "+-1234567890":
        idx = 1
        while idx < len(inp) and inp[idx] in "1234567890., ":
            idx += 1

        val_part, curr_part = inp[:idx].strip(), inp[idx:].strip()

    # currency start with symbol
    else:
        idx = len(inp) - 1
        while idx > 0 and inp[idx] in "-+1234567890., ":
            idx -= 1

        val_part, curr_part = inp[idx + 1 :].strip(), inp[: idx + 1].strip()

    if curr_part != '""':
        if curr_part and (err2 := _validate_currency(curr_part)):
            raise ValueError(err2)

        if len(curr_part) > 2 and curr_part[0] == curr_part[-1] == '"':  # noqa:PLR2004
            curr_part = curr_part[1:-1]

    return val_part, curr_part


def parse_amount(ctx: model.Context, inp: str) -> model.Price:
    val_part, curr_part = _parse_amount_split_parts(inp)
    decimal_mark = None
    if comm := ctx.commodities.get(curr_part or ctx.default_commodity):
        decimal_mark = comm.decimal_mark

    try:
        val = parse_float(ctx, val_part, decimal_mark)
    except InvalidOperation as err:
        msg = f"invalid value `{val_part}`"
        raise ValueError(msg) from err

    if val is None:
        raise errors.MissingValueError

    if curr_part:
        return model.Price(inp, val, curr_part)

    return model.Price(
        inp,
        val,
        ctx.default_commodity,
        use_default_currency=True,
    )
