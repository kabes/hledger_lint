# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

""" """

import string
import typing as ty
from decimal import Decimal

from hledger_lint import model

from . import common, errors
from .amount import _parse_amount_split_parts


def _count_separators(instr: str, decimal_mark: str) -> ty.Iterable[str]:
    for s in ",'. ":
        if s == decimal_mark:
            continue

        if sum(1 for c in instr if c == s) > 0:
            yield s


def prepare_commodity_format(fmt: str) -> tuple[Decimal, str, str]:
    """Try guess  decimal point & format for commodity"""
    if not fmt:
        raise ValueError("empty commodity format")

    if fmt[-1] in ",.":
        decimal_mark = fmt[-1]
    else:
        # find decimal separator - it last '.' or ',' in string
        try:
            decimal_mark = next(c for c in reversed(fmt) if c in ",.")
        except StopIteration:
            # no decimal point defined
            raise ValueError("no decimal mark found") from None

    # remove non-numeric and not decimal-separator characters
    int_part, _, frac_part = fmt.rpartition(decimal_mark)

    if decimal_mark in int_part:
        raise ValueError(
            "can't parse commodity format - decimal mark not detected"
        )

    # detect thousands_sep
    match list(_count_separators(int_part, decimal_mark)):
        case [c]:
            thousands_sep = c
        case []:
            thousands_sep = ""
        case _:
            raise ValueError(
                "can't parse commodity format - invalid thousands separator"
            )

    int_part = "".join(c for c in int_part if c in string.digits)
    frac_part = "".join(c for c in frac_part if c in string.digits)

    return Decimal(f"{int_part}.{frac_part}"), decimal_mark, thousands_sep


def parse_commodity(
    ctx: model.Context,  # noqa: ARG001
    line: str,
    line_no: int,
) -> model.Commodity | model.ParseError:
    # commodity 100.000,00 EDO
    raw = line
    prefix, _sep, line = line.partition(" ")
    com_default = prefix.lower() == "d"

    line = common.strip_comment(line)
    if not line:
        raise errors.ParseError("empty line")

    try:
        val_part, curr_part = _parse_amount_split_parts(line)
    except ValueError as err:
        errmsg = f"parse price error: {err}"
        raise errors.ParseError(errmsg) from err

    if not val_part:
        raise errors.ParseError("parse price error: missing format")

    if not curr_part:
        raise errors.ParseError("parse price error: missing currency")

    comm = model.Commodity(
        line_no=line_no,
        raw=raw,
        price_format=line,
        currency=curr_part,
        default=com_default,
    )

    try:
        comm.decimal, comm.decimal_mark, comm.thousands_sep = (
            prepare_commodity_format(val_part)
        )
    except ValueError as err:
        errmsg = f"parse price error: {err}"
        raise errors.ParseError(errmsg) from err

    return comm
