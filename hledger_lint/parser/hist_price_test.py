# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from datetime import date
from decimal import Decimal

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model, parser

from . import errors


class TestParseHistPrice:
    def test_parse_hist_price(self):
        ctx = model.Context(conf.Config())

        inp = "P 2000-11-21 DOS 100.12 PLN"
        hpr = parser.parse_hist_price(ctx, inp, 1)
        assert isinstance(hpr, model.HistPrice)
        assert hpr.raw == inp
        assert hpr.line_no == 1
        assert hpr.date == date(2000, 11, 21)
        assert hpr.commodity == "DOS"
        assert hpr.price == model.Price("100.12 PLN", Decimal("100.12"), "PLN")

        inp = 'P 2000-11-21 DOS 100 000.00 "abc sd"'
        hpr = parser.parse_hist_price(ctx, inp, 1)
        assert isinstance(hpr, model.HistPrice)
        assert hpr.raw == inp
        assert hpr.line_no == 1
        assert hpr.date == date(2000, 11, 21)
        assert hpr.commodity == "DOS"
        assert hpr.price == model.Price(
            '100 000.00 "abc sd"', Decimal("100000.00"), "abc sd"
        )

        inp = 'P 2000-11-21 "DOS adas" 100 000.00 "abc sd"'
        hpr = parser.parse_hist_price(ctx, inp, 1)
        assert isinstance(hpr, model.HistPrice)
        assert hpr.raw == inp
        assert hpr.line_no == 1
        assert hpr.date == date(2000, 11, 21)
        assert hpr.commodity == "DOS adas"
        assert hpr.price == model.Price(
            '100 000.00 "abc sd"', Decimal("100000.00"), "abc sd"
        )

        inp = 'P 2000-11-21 "DOS adas" 100 000.00 PLN'
        hpr = parser.parse_hist_price(ctx, inp, 1)
        assert isinstance(hpr, model.HistPrice)
        assert hpr.raw == inp
        assert hpr.line_no == 1
        assert hpr.date == date(2000, 11, 21)
        assert hpr.commodity == "DOS adas"
        assert hpr.price == model.Price(
            "100 000.00 PLN", Decimal("100000.00"), "PLN"
        )

    def test_parse_invalid(self):
        ctx = model.Context(conf.Config())

        inp = "P 2000-21-01 DOS 100.12 PLN"
        with pytest.raises(errors.ParseError, match="invalid date"):
            parser.parse_hist_price(ctx, inp, 1)

        inp = "P 2000-01-01"
        with pytest.raises(errors.ParseError, match="invalid format"):
            parser.parse_hist_price(ctx, inp, 10)

        inp = "P 2000-01-01 PLN"
        with pytest.raises(errors.ParseError, match="missing price"):
            parser.parse_hist_price(ctx, inp, 10)
