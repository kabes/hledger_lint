# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

import logging
import typing as ty

from hledger_lint import model

from . import errors, other as parse_other
from .account import parse_account
from .commodity import parse_commodity, prepare_commodity_format
from .common import NOOP
from .hist_price import parse_hist_price
from .transaction import parse_transaction

__all__ = ["prepare_commodity_format", "parse", "parse_conf"]

_LOG = logging.getLogger("parser")


# Line with it number
LineWNo = tuple[int, str]
ILines = ty.Iterable[str]
Segment = list[LineWNo]

ParseResult = ty.Union[
    model.Account,
    model.Commodity,
    model.HistPrice,
    model.Tag,
    model.Payee,
    model.ParseError,
    model.DecimalMarkDirective,
    model.YearDirective,
    object,
    None,
]


def _parse_segment(ctx: model.Context, lines: Segment) -> ParseResult:
    lineno, line = lines[0]
    line_prefix, _sep, rest = line.partition(" ")

    res: ParseResult = None

    try:
        match line_prefix.lower():
            case "account":
                res = parse_account(ctx, line, lineno)

            case "commodity" | "d":
                res = parse_commodity(ctx, line, lineno)

            case "p":
                res = parse_hist_price(ctx, line, lineno)

            case "tag":
                res = parse_other.parse_tag(ctx, line, lineno)

            case "payee":
                res = parse_other.parse_payee(ctx, line, lineno)

            case "decimal-mark":
                res = parse_other.parse_decimal_mark(rest)

            case "y":
                res = parse_other.parse_year(rest)
    except errors.ParseError as err:
        res = model.ParseError(lineno, line, str(err))

    return res


def _skip_comments(lines: ILines) -> ty.Iterable[LineWNo]:
    in_comment = False
    for lineno, inline in enumerate(lines, 1):
        line = inline.rstrip()

        if not line:
            continue

        if line == "comment":
            in_comment = True
            continue

        if in_comment:
            if line == "end comment":
                in_comment = False

            continue

        if line[0] in ("#", ";", "*"):
            continue

        # replace tabs by 4 space to simplify parsing
        line = line.expandtabs(4)

        yield lineno, line


def _split_segments(lines: ty.Iterable[LineWNo]) -> ty.Iterable[Segment]:
    curr_seg_lines: list[LineWNo] = []
    for lineno, line in lines:
        if line[0] != " " and curr_seg_lines:
            yield curr_seg_lines
            curr_seg_lines = []

        curr_seg_lines.append((lineno, line))

    if curr_seg_lines:
        yield curr_seg_lines


def parse(ctx: model.Context, lines: ILines) -> ty.Iterable[ty.Any]:
    for seg_lines in _split_segments(_skip_comments(lines)):
        if res := _parse_segment(ctx, seg_lines):
            if res != NOOP:
                yield res
        else:
            trans = parse_transaction(ctx, seg_lines)
            if isinstance(trans, model.Transaction):
                for num in trans.numbers:
                    if num in ctx.transaction_numbers:
                        continue

                    ctx.transaction_numbers[num] = model.TransNumberInfo(
                        trans.line_no,
                    )

            yield trans


def parse_conf(ctx: model.Context, lines: ILines) -> ty.Iterable[ty.Any]:
    """Fast parse configuration from hledger file; skip transactions."""

    for seg_lines in _split_segments(_skip_comments(lines)):
        if (res := _parse_segment(ctx, seg_lines)) != NOOP:
            yield res
