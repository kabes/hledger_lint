# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from decimal import Decimal

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model
from hledger_lint.parser import amount as a_parser


class TestParseFloat:
    def test_parse_float0(self):
        cfg = conf.Config()
        cfg.formatting.decimal_mark = "."
        cfg.formatting.thousands_sep = ","
        ctx = model.Context(cfg)

        assert a_parser.parse_float(ctx, "") is None

    @pytest.mark.parametrize(
        ("data", "expected"),
        [
            ("123,322.12", Decimal("123322.12")),
            ("123322.12", Decimal("123322.12")),
            ("-12.12", Decimal("-12.12")),
            ("- 12.12", Decimal("-12.12")),
            ("+   12.12", Decimal("12.12")),
        ],
    )
    def test_parse_float1(self, data, expected):
        cfg = conf.Config()
        cfg.formatting.decimal_mark = "."
        cfg.formatting.thousands_sep = ","
        ctx = model.Context(cfg)
        assert a_parser.parse_float(ctx, data) == expected

    @pytest.mark.parametrize(
        ("data", "expected"),
        [
            ("123.322,12", Decimal("123322.12")),
            ("123322,12", Decimal("123322.12")),
            ("-12,12", Decimal("-12.12")),
        ],
    )
    def test_parse_float2(self, data, expected):
        cfg = conf.Config()
        cfg.formatting.decimal_mark = ","
        cfg.formatting.thousands_sep = "."
        ctx = model.Context(cfg)
        assert a_parser.parse_float(ctx, data) == expected


class TestParseAmount:
    @pytest.mark.parametrize(
        ("test_data", "decimal", "currency", "isdefault"),
        [
            ("123.32", Decimal("123.32"), "", True),
            ("123", Decimal("123.0"), "", True),
            ("-1.23", Decimal("-1.23"), "", True),
            ('-1.23 ""', Decimal("-1.23"), '""', False),
        ],
    )
    def test_parse_amount_no_curr(
        self, test_data, decimal, currency, isdefault
    ):
        ctx = model.Context(conf.Config())
        assert a_parser.parse_amount(ctx, test_data) == model.Price(
            test_data, decimal, currency, isdefault
        )

    @pytest.mark.parametrize(
        ("test_data", "decimal", "currency", "isdefault"),
        [
            ("123.32", Decimal("123.32"), "PLN", True),
            ("123", Decimal("123.0"), "PLN", True),
            ("-1.23", Decimal("-1.23"), "PLN", True),
            ('-1.23 ""', Decimal("-1.23"), '""', False),
        ],
    )
    def test_parse_amount_no_curr_default(
        self, test_data, decimal, currency, isdefault
    ):
        ctx = model.Context(conf.Config())
        ctx.default_commodity = "PLN"
        assert a_parser.parse_amount(ctx, test_data) == model.Price(
            test_data, decimal, currency, isdefault
        )

    @pytest.mark.parametrize(
        ("test_data", "decimal", "currency"),
        [
            ("$123.32", Decimal("123.32"), "$"),
            ("$ -123.32", Decimal("-123.32"), "$"),
            ("ABC 123", Decimal(123), "ABC"),
        ],
    )
    def test_parse_amount_curr_before(self, test_data, decimal, currency):
        ctx = model.Context(conf.Config())
        assert a_parser.parse_amount(ctx, test_data) == model.Price(
            test_data, decimal, currency
        )

    @pytest.mark.parametrize(
        ("data", "expected"),
        [
            ("123.32EUR", model.Price("123.32EUR", Decimal("123.32"), "EUR")),
            (
                "-123.32 PLN",
                model.Price("-123.32 PLN", Decimal("-123.32"), "PLN"),
            ),
            ("-123 PLN", model.Price("-123 PLN", Decimal("-123.00"), "PLN")),
            ("1234 TOS", model.Price("1234 TOS", Decimal("1234.00"), "TOS")),
        ],
    )
    def test_parse_amount_curr_after(self, data, expected):
        ctx = model.Context(conf.Config())
        assert a_parser.parse_amount(ctx, data) == expected

    @pytest.mark.parametrize(
        ("data", "expected"),
        [
            ("1234 TOS", model.Price("1234 TOS", Decimal("1234.00"), "TOS")),
            (
                "1234,123 TOS",
                model.Price("1234,123 TOS", Decimal("1234.123"), "TOS"),
            ),
            (
                "-123,12 PLN",
                model.Price("-123,12 PLN", Decimal("-123.12"), "PLN"),
            ),
            ("-123 PLN", model.Price("-123 PLN", Decimal("-123.00"), "PLN")),
        ],
    )
    def test_parse_amount_curr_after_decimal_point(self, data, expected):
        ctx = model.Context(conf.Config())
        ctx.decimal_mark = ","

        assert a_parser.parse_amount(ctx, data) == expected

    @pytest.mark.parametrize(
        ("test_data", "decimal", "currency"),
        [
            ('123.32 "dma da"', Decimal("123.32"), "dma da"),
            ('123.32"d32 ma da"', Decimal("123.32"), "d32 ma da"),
            ('"d32 ma da" 123.32', Decimal("123.32"), "d32 ma da"),
            ('"d32 ma da"123.32', Decimal("123.32"), "d32 ma da"),
        ],
    )
    def test_parse_amount_quoted(self, test_data, decimal, currency):
        ctx = model.Context(conf.Config())
        assert a_parser.parse_amount(ctx, test_data) == model.Price(
            test_data, decimal, currency
        )

    def test_parse_amount_invalid(self):
        ctx = model.Context(conf.Config())

        with pytest.raises(ValueError, match="currency has inner"):
            a_parser.parse_amount(ctx, '123.32 "dma "da"')

        with pytest.raises(ValueError, match="too short"):
            a_parser.parse_amount(ctx, '123.32 "')

        with pytest.raises(ValueError, match="missing closing"):
            a_parser.parse_amount(ctx, '" 123.32')

    def test_parse_amount_commodity(self):
        ctx = model.Context(conf.Config())
        ctx.decimal_mark = "."
        ctx.commodities["PLN"] = model.Commodity(
            "",
            0,
            "PLN",
            "100,00 PLN",
            decimal_mark=",",
            decimal=Decimal("100.00"),
        )

        assert a_parser.parse_amount(ctx, "1234 PLN") == model.Price(
            "1234 PLN",
            Decimal("1234.00"),
            "PLN",
        )

        assert a_parser.parse_amount(ctx, "1234,123 PLN") == model.Price(
            "1234,123 PLN",
            Decimal("1234.123"),
            "PLN",
        )

        ctx.decimal_mark = ""

        assert a_parser.parse_amount(ctx, "-123,12 PLN") == model.Price(
            "-123,12 PLN",
            Decimal("-123.12"),
            "PLN",
        )

        assert a_parser.parse_amount(ctx, "-123 PLN") == model.Price(
            "-123 PLN",
            Decimal("-123.00"),
            "PLN",
        )

        ctx.commodities["EUR"] = model.Commodity(
            "",
            0,
            "EUR",
            "100,000.00 EUR",
            decimal_mark=".",
            decimal=Decimal("100000.00"),
        )

        assert a_parser.parse_amount(ctx, "-123,00 EUR") == model.Price(
            "-123,00 EUR",
            Decimal("-12300"),
            "EUR",
        )

    def test_parse_amount_commodity_invalid(self):
        ctx = model.Context(conf.Config())
        ctx.decimal_mark = "."
        ctx.commodities["PLN"] = model.Commodity(
            "",
            0,
            "PLN",
            "100,00 PLN",
            decimal_mark=",",
            decimal=Decimal("1200.00"),
        )

        # with pytest.raises(ValueError) as excinfo:
        #    a_parser.parse_amount(ctx, "12,234.00 PLN")


class TestValidateCurrency:
    @pytest.mark.parametrize(
        ("test_data", "expected"),
        [
            ('""', None),
            ('"', "too short"),
            ('"abc', 'missing or wrong closing `"`'),
            ('" dkal" dlaskd"333', 'missing or wrong closing `"`'),
            ('""" cc', 'missing or wrong closing `"`'),
            ("PLN", None),
            ('"some currency"', None),
            ("some currency", "spaces in unquoted currency"),
            ('"some " "currency"', 'currency has inner `"`'),
            ('"some " currency"', 'currency has inner `"`'),
            ('" "', "too short"),
            ("ab,", "invalid char in commodity"),
            ("ab<", "invalid char in commodity"),
        ],
    )
    def test_validate(self, test_data, expected):
        assert a_parser._validate_currency(test_data) == expected
