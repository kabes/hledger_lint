# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

""" """

from contextlib import suppress
from datetime import date, datetime

from hledger_lint import model

# skip result
NOOP = object()


_DATE_FORMATS = [
    "%Y-%m-%d",
    "%Y/%m/%d",
    "%y-%m-%d",
    "%y/%m/%d",
    "%Y.%m.%d",
    "%y.%m.%d",
    "%m/%d",
    "%m-%d",
    "%m.%d",
]


def strip_comment(instr: str, *sep: str, min_pos: int = 0) -> str:
    if not sep:
        sep = (";", "#")

    while (idx := max(instr.rfind(s) for s in sep)) > min_pos:
        instr = instr[:idx].rstrip()

    return instr


def parse_date(ctx: model.Context, inp: str) -> date:
    inp = inp.strip()

    if ctx.conf.formatting.date != "auto":
        try:
            return datetime.strptime(inp, ctx.conf.formatting.date).date()
        except ValueError as err:
            errmsg = f"parse date {inp} error"
            raise ValueError(errmsg) from err

    for dformat in _DATE_FORMATS:
        with suppress(ValueError):
            d = datetime.strptime(inp, dformat).date()
            if "%y" not in dformat and "%Y" not in dformat:
                d = d.replace(year=ctx.current_year or date.today().year)

            return d

    errmsg = f"parse date {inp} error"
    raise ValueError(errmsg)
