# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from decimal import Decimal

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model
from hledger_lint.parser import commodity as c_parser

from . import errors


class TestParseCommodity:
    def test_parse_commodity(self):
        ctx = model.Context(conf.Config())
        com = c_parser.parse_commodity(ctx, "commodity 1231231.32 PLN", 13)
        assert isinstance(com, model.Commodity), com
        assert com.raw == "commodity 1231231.32 PLN"
        assert com.line_no == 13
        assert com.currency == "PLN"
        assert com.price_format == "1231231.32 PLN"
        assert not com.default
        assert com.decimal_mark == "."
        assert com.decimal == Decimal("1231231.32")

    def test_parse_commodity_comments(self):
        ctx = model.Context(conf.Config())
        com = c_parser.parse_commodity(
            ctx,
            "commodity 1231231.32 PLN ; test",
            13,
        )
        assert isinstance(com, model.Commodity)
        assert com.line_no == 13
        assert com.currency == "PLN"
        assert com.price_format == "1231231.32 PLN"

        com = c_parser.parse_commodity(
            ctx,
            "commodity 1231231.32 PLN  # test",
            13,
        )
        assert isinstance(com, model.Commodity)
        assert com.line_no == 13
        assert com.currency == "PLN"
        assert com.price_format == "1231231.32 PLN"

    def test_parse_commodity_default(self):
        ctx = model.Context(conf.Config())
        com = c_parser.parse_commodity(ctx, "D $12312.32", 13)
        assert isinstance(com, model.Commodity)
        assert com.raw == "D $12312.32"
        assert com.line_no == 13
        assert com.currency == "$"
        assert com.price_format == "$12312.32"
        assert com.default

    def test_parse_commodity_fail(self):
        ctx = model.Context(conf.Config())

        with pytest.raises(errors.ParseError):
            c_parser.parse_commodity(ctx, "D 123 eqwk 123123", 13)

        with pytest.raises(errors.ParseError):
            c_parser.parse_commodity(ctx, "commodity EUR", 13)

    def test_parse_commodity_quoted(self):
        ctx = model.Context(conf.Config())

        com = c_parser.parse_commodity(ctx, 'commodity 1231231.32 "ab c"', 13)
        assert isinstance(com, model.Commodity)
        assert com.raw == 'commodity 1231231.32 "ab c"'
        assert com.currency == "ab c"
        assert com.price_format == '1231231.32 "ab c"'

        com = c_parser.parse_commodity(ctx, 'commodity "ab c"  1231231.32', 13)
        assert isinstance(com, model.Commodity)
        assert com.raw == 'commodity "ab c"  1231231.32'
        assert com.currency == "ab c"
        assert com.price_format == '"ab c"  1231231.32'

    @pytest.mark.parametrize(
        ("data", "decimal_mark", "decimal"),
        [
            ('commodity "ab c"  100,00', ",", Decimal("100.00")),
            ("commodity EUR 1 00,00", ",", Decimal("100.00")),
            ("commodity EUR 10.00,000", ",", Decimal("1000.000")),
            ("commodity EUR 10,00.000", ".", Decimal("1000.000")),
            ("commodity EUR  10 000.000", ".", Decimal("10000.000")),
        ],
    )
    def test_parse_commodity_guess_mark(self, data, decimal_mark, decimal):
        ctx = model.Context(conf.Config())
        com = c_parser.parse_commodity(ctx, data, 1)
        assert isinstance(com, model.Commodity)
        assert com.decimal_mark == decimal_mark
        assert com.decimal == decimal


class TestPrepareComodityFormat:
    @pytest.mark.parametrize(
        ("test_data", "expected"),
        [
            ("100,00", (Decimal("100.00"), ",", "")),
            ("100.00", (Decimal("100.00"), ".", "")),
            ("100 000.00", (Decimal("100000.00"), ".", " ")),
            ("100,000.00", (Decimal("100000.00"), ".", ",")),
            ("100.000,00", (Decimal("100000.00"), ",", ".")),
            ("100.000.000,00", (Decimal("100000000.00"), ",", ".")),
            ("100.", (Decimal("100"), ".", "")),
            ("100,", (Decimal("100"), ",", "")),
        ],
    )
    def test_prepare(self, test_data, expected):
        assert c_parser.prepare_commodity_format(test_data) == expected

    @pytest.mark.parametrize(
        ("test_data"),
        [
            "10.0.00,00,00",
            " 1'00.00,00",
            "1'00.00,00",
            "1 00.00,00",
            "1 00.00,00.",
            "1 00.00.00.",
            "100",  # any decimal point must exists
            "1 000",  # any decimal point must exists
        ],
    )
    def test_prepare_fail(self, test_data):
        with pytest.raises(ValueError):
            c_parser.prepare_commodity_format(test_data)
