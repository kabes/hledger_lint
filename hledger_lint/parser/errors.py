# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsing errors definition.
"""

__all__ = ["ParseError", "InvalidFormatError", "MissingValueError"]


class ParseError(ValueError):
    def __init__(self, msg: str) -> None:
        super().__init__(msg)


class InvalidFormatError(ParseError):
    def __init__(self) -> None:
        super().__init__("invalid format")


class MissingValueError(ValueError):
    def __init__(self) -> None:
        super().__init__("missing value")
