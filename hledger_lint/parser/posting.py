# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

from hledger_lint import hledger, model

from . import common
from .amount import parse_amount


def _acc_is_not_balancing(account: str) -> bool:
    return (account[0] == "(" and account[-1] == ")") or (
        account[0] == "[" and account[-1] == "]"
    )


def _try_parse_amount(ctx: model.Context, instr: str) -> model.Price | None:
    instr = instr.strip()
    try:
        return parse_amount(ctx, instr)
    except ValueError as err:
        msg = f"parse amount `{instr}` error: {err}"
        raise ValueError(msg) from err


def _parse_prices(
    ctx: model.Context,
    amcur: str,
) -> tuple[model.Price | None, model.Price | None, model.Price | None]:
    """Try parse price in format '<val> <commodity>' or
    '<val> <commodity> @@ <total price> <commodity>' or
    '<val> <commodity> @ <unit price> <commodity>'.
    Return tuple (<amount>, <total price>, <unint price>).
    Raise ValueError on parsing error
    """

    amount_str = amcur
    total_price = None
    unit_price = None

    if "@@" in amcur:
        amount_str, _sep, total = amcur.partition("@@")
        total_price = _try_parse_amount(ctx, total)

    elif "@" in amcur:
        amount_str, _sep, total = amcur.partition("@")
        unit_price = _try_parse_amount(ctx, total)

    amount = _try_parse_amount(ctx, amount_str)

    return (amount, total_price, unit_price)


def _try_parse_assertion(
    ctx: model.Context,
    bal_assert: str,
) -> model.Assertion:
    # bal_assert not contain leading '='
    assert_type = model.AssertionType.NORMAL

    # strong subaccounts-inclusive
    if bal_assert.startswith("=*"):
        assert_type = model.AssertionType.SUBACCOUNT_INCLUSIVE_STRONG
        bal_assert = bal_assert[2:]
    elif bal_assert.startswith("="):
        assert_type = model.AssertionType.STRONG
        bal_assert = bal_assert[1:]
    elif bal_assert.startswith("*"):
        assert_type = model.AssertionType.SUBACCOUNT_INCLUSIVE
        bal_assert = bal_assert[1:]

    if price := _try_parse_amount(ctx, bal_assert):
        return model.Assertion.from_price(price, assert_type)

    msg = f"invalid assertion amount: {bal_assert}"
    raise ValueError(msg)


def parse_posting(
    ctx: model.Context,
    line_no: int,
    line: str,
) -> model.Posting | model.ParseError:
    raw = line

    # strip comments
    if (min_pos := line.find("  ", 4)) > 0:
        line = common.strip_comment(line, min_pos=min_pos)

    line = line.strip()

    account_end = line.find("  ")
    if account_end < 0:
        posting = model.Posting(raw=raw, line_no=line_no, account=line.strip())
    else:
        account = line[:account_end]
        is_not_balancing = _acc_is_not_balancing(account)
        if is_not_balancing:
            account = account[1:-1]

        posting = model.Posting(
            raw=raw,
            line_no=line_no,
            account=account,
            is_not_balancing=is_not_balancing,
        )
        line = line[account_end:].strip()
        amcur, _, bal_assert = line.partition("=")

        try:
            if amcur := amcur.strip():
                posting.amount, posting.total_price, posting.unit_price = (
                    _parse_prices(ctx, amcur)
                )

            if bal_assert := bal_assert.strip():
                posting.balance_assertion = _try_parse_assertion(
                    ctx,
                    bal_assert,
                )

        except ValueError as err:
            return model.ParseError(line_no, raw, str(err))

    posting.account_type = (
        ctx.conf.find_type_for_account(posting.account)
        or hledger.guess_account_type(posting.account)
        or hledger.AccountType.UNKNOWN
    )

    return posting
