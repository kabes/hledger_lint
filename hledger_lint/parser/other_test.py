# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress

import pytest

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model
from hledger_lint.parser import other as o_parser

from . import errors


class TestParsePayee:
    @pytest.mark.parametrize(
        ("test_input", "expected"),
        [
            ('payee ""', ""),
            ("payee payeename", "payeename"),
            ("payee payee name", "payee name"),
            ("payee payee name; with semic", "payee name; with semic"),
            (
                "payee payee name; with semic  ; comment",
                "payee name; with semic",
            ),
        ],
    )
    def test_parse_payee_ok(self, test_input, expected):
        ctx = model.Context(conf.Config())
        assert o_parser.parse_payee(ctx, test_input, 0) == model.Payee(
            test_input, 0, expected
        )

    @pytest.mark.parametrize(
        "test_input",
        [
            "payee ",
            "payee  ; comment",
            "payee    ; comment",
        ],
    )
    def test_parse_payee_nok(self, test_input):
        ctx = model.Context(conf.Config())
        with pytest.raises(errors.ParseError, match="invalid format"):
            o_parser.parse_payee(ctx, test_input, 10)

    def test_parse_tag(self):
        ctx = model.Context(conf.Config())
        inp = "tag tagname"
        assert o_parser.parse_tag(ctx, inp, 10) == model.Tag(
            inp, 10, "tagname"
        )

        inp = "tag "

        with pytest.raises(errors.ParseError, match="invalid format"):
            o_parser.parse_tag(ctx, inp, 10)


def test_parse_decimal_mark():
    assert o_parser.parse_decimal_mark(",") == model.DecimalMarkDirective(",")
    assert o_parser.parse_decimal_mark(".") == model.DecimalMarkDirective(".")

    with pytest.raises(errors.ParseError, match="invalid decimal mark"):
        o_parser.parse_decimal_mark("")
    with pytest.raises(errors.ParseError, match="invalid decimal mark"):
        o_parser.parse_decimal_mark("'")
    with pytest.raises(errors.ParseError, match="invalid decimal mark"):
        o_parser.parse_decimal_mark("abc")


def test_parse_year():
    assert o_parser.parse_year("2020") == model.YearDirective(2020)

    with pytest.raises(errors.ParseError, match="invalid year"):
        o_parser.parse_year("1900")

    with pytest.raises(errors.ParseError, match="invalid year"):
        o_parser.parse_year("")
