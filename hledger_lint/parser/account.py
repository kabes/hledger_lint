# Copyright © 2022-2024, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

"""
Parsers for various part of hledger journal file.
"""

import re

from hledger_lint import hledger, model

from . import errors

_ACCOUNT_TYPE_RE = re.compile(r"\btype:([a-zA-Z]+)\b")


def parse_account(
    ctx: model.Context,
    line: str,
    line_no: int,
) -> model.Account:
    # account Aktywa      ; type:Asset
    raw = line
    line = line[8:].strip()
    if not line or line[0] in ";#":
        raise errors.ParseError("missing account name")

    account = model.Account(raw=raw, line_no=line_no, account=line)
    account_type: hledger.AccountType | None = None
    # for account comment must start with two spaces
    name, _sep, comment = line.partition("  ;")
    if comment:
        account.account = name.strip()
        if actype_s := _ACCOUNT_TYPE_RE.search(comment):
            account_type = hledger.expand_type_shortcut(
                actype_s.group(1).lower()
            )

    if account_type is None:
        account_type = ctx.conf.find_type_for_account(account.account)

    if account_type is None:
        account_type = hledger.guess_account_type(account.account)

    if account_type is not None:
        account.account_type = account_type

    return account
