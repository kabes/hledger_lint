# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from decimal import Decimal

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model
from hledger_lint.parser import posting as p_parser


class TestParsePosting:
    def test_parse1(self):
        ctx = model.Context(conf.Config())
        inp = (
            "    Aktywa:Aktywa bieżące:ROR                          "
            "-346.45 PLN"
        )
        ent = p_parser.parse_posting(ctx, 123, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.line_no == 123
        assert ent.account == "Aktywa:Aktywa bieżące:ROR"
        assert ent.amount.amount == Decimal("-346.45")
        assert ent.amount.currency == "PLN"
        assert not ent.is_not_balancing

    def test_parse_comments(self):
        ctx = model.Context(conf.Config())
        inp = "Przychód:Artykuły spożywcze   +12312.232 PLN ; lwkeql"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.line_no == 12
        assert ent.account == "Przychód:Artykuły spożywcze"
        assert ent.amount.amount == Decimal("12312.232")
        assert ent.amount.currency == "PLN"
        assert not ent.is_not_balancing

        inp = "Przychód:Artykuły spożywcze  ; lwkeql"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.line_no == 12
        assert ent.account == "Przychód:Artykuły spożywcze"

        inp = "Przychód:Artykuły spożywcze   +12312.232 PLN  # lwkeql"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.line_no == 12
        assert ent.account == "Przychód:Artykuły spożywcze"
        assert ent.amount.amount == Decimal("12312.232")
        assert ent.amount.currency == "PLN"

        inp = "Przychód:Artykuły spożywcze   # lwkeql"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.line_no == 12
        assert ent.account == "Przychód:Artykuły spożywcze"

        inp = "Przychód:Artykuły spożywcze   +12312.232 PLN ; lwkeql; abc"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.account == "Przychód:Artykuły spożywcze"

        inp = "Przychód:Artykuły spożywcze  ; lwkeql; abc"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.account == "Przychód:Artykuły spożywcze"

        inp = (
            "Assets:US:ETrade:VHT                                   "
            "-73.00 VHT      @ 46.42 USD      ;@ 44.39 USD ;   "
            "-3388.66 USD"
        )
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.account == "Assets:US:ETrade:VHT"
        assert ent.amount.amount == Decimal("-73.00")
        assert ent.amount.currency == "VHT"

        inp = "Przychód:Artykuły spożywcze   +12312.232 PLN; lwkeql; abc"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.account == "Przychód:Artykuły spożywcze"

        inp = "Przychód:Artykuły spożywcze   ; lwkeql; abc"
        ent = p_parser.parse_posting(ctx, 12, inp)
        assert isinstance(ent, model.Posting)
        assert ent.raw == inp
        assert ent.account == "Przychód:Artykuły spożywcze"

    def test_parse3(self):
        ctx = model.Context(conf.Config())
        inp = "  (Przychód:abc)   -12312.232"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.line_no == 2
        assert ent.account == "Przychód:abc"
        assert ent.amount.amount == Decimal("-12312.232")
        assert ent.amount.currency == ""
        assert ent.is_not_balancing

        inp = "  [Przychód:abc]   -12312.232"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.raw == inp
        assert ent.line_no == 2
        assert ent.account == "Przychód:abc"
        assert ent.amount.amount == Decimal("-12312.232")
        assert ent.amount.currency == ""
        assert ent.is_not_balancing

    def test_parse_total(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   123.02 PLN @@ 543.12 EUR = 232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.account == "Przychód:abc"
        assert ent.amount.amount == Decimal("123.02")
        assert ent.amount.currency == "PLN"
        assert ent.total_price
        assert ent.total_price.amount == Decimal("543.12")
        assert ent.total_price.currency == "EUR"
        assert ent.unit_price is None

    def test_parse_unit_price(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR = -232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.amount
        assert ent.account == "Przychód:abc"
        assert ent.amount.amount == Decimal("-123.02")
        assert ent.amount.currency == "PLN"
        assert ent.unit_price
        assert ent.unit_price.amount == Decimal("-5.12")
        assert ent.unit_price.currency == "EUR"
        assert ent.total_price is None

    def test_parse_assertion(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR = -232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("-232.12")
        assert ent.balance_assertion.currency == "PLN"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.NORMAL
        )

        inp = "  Przychód:abc  = 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.NORMAL
        )

        inp = "  Przychód:abc   -123.02 PLN = 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.NORMAL
        )

        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR = 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.NORMAL
        )

        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion is None

        inp = "  Przychód:abc   -123.02 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion is None

        inp = "  Przychód:abc"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion is None

    def test_parse_assertion_strong(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR == -232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("-232.12")
        assert ent.balance_assertion.currency == "PLN"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.STRONG
        )

        inp = "  Przychód:abc  == 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.STRONG
        )

        inp = "  Przychód:abc   -123.02 PLN == 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.STRONG
        )

        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR == 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type == model.AssertionType.STRONG
        )

    def test_parse_assertion_sa(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR =* -232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("-232.12")
        assert ent.balance_assertion.currency == "PLN"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE
        )

        inp = "  Przychód:abc  =* 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE
        )

        inp = "  Przychód:abc   -123.02 PLN =* 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE
        )

        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR = 232.12 EUR"
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE
        )

    def test_parse_assertion_sa_strong(self):
        ctx = model.Context(conf.Config())
        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR ==* -232.12 PLN"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("-232.12")
        assert ent.balance_assertion.currency == "PLN"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE_STRONG
        )

        inp = "  Przychód:abc  ==* 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE_STRONG
        )

        inp = "  Przychód:abc   -123.02 PLN ==* 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.amount == Decimal("232.12")
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE_STRONG
        )

        inp = "  Przychód:abc   -123.02 PLN @ -5.12 EUR ==* 232.12 EUR"
        ent = p_parser.parse_posting(ctx, 2, inp)
        assert isinstance(ent, model.Posting)
        assert ent.balance_assertion
        assert ent.balance_assertion.currency == "EUR"
        assert (
            ent.balance_assertion.assertion_type
            == model.AssertionType.SUBACCOUNT_INCLUSIVE_STRONG
        )
