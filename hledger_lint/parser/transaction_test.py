# Copyright © 2022-2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code="no-untyped-def,attr-defined"
# ruff: noqa: ANN201,PLR2004,FBT003,PLR0911,PLR0915,ANN001

from contextlib import suppress
from datetime import date

with suppress(ImportError):
    import icecream

    icecream.install()
    icecream.ic.configureOutput(includeContext=True)

from hledger_lint import conf, model, parser


class TestParseRecord:
    def test_parse_comments(self):
        ctx = model.Context(conf.Config())
        inp = [
            "2021-07-14 something  ; a transaction comment",
            "    ; the transaction comment, continued",
            "    posting1  1  ; a comment for posting 1",
            "    posting2",
            "    ; a comment for posting 2",
            "    ; another comment line for posting 2",
            "; a file comment (because not indented)",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something"
        assert tran.date == date(2021, 7, 14)
        assert tran.postings
        assert len(tran.postings) == 2
        assert isinstance(tran.postings[0], model.Posting)
        assert tran.postings[0].account == "posting1"
        assert isinstance(tran.postings[1], model.Posting)
        assert tran.postings[1].account == "posting2"

        inp = [
            "2021-07-14 something   # a transaction comment",
            "    posting1  1  ; a comment for posting 1",
            "    posting2",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something"

        inp = ["2021-07-14", "    posting2"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == ""

        inp = ["2021-07-14 ; test", "    posting2"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "; test"

    def test_parse_tags(self):
        ctx = model.Context(conf.Config())
        inp = [
            "2021-07-14 something  ; a transaction comment",
            "    ; the transaction-tag2:, transaction-tag1: comment,",
            "    posting1  1  ; tag:, a comment for posting 1",
            "    posting2  ; tag:",
            "    ; a comment for posting 2; tag:, tag2:, test",
            "    ; another comment line for posting 2",
            "; a file comment (because not indented)",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something"
        assert tran.date == date(2021, 7, 14)
        assert tran.postings
        assert len(tran.postings) == 2
        assert isinstance(tran.postings[0], model.Posting)
        assert tran.postings[0].account == "posting1"
        assert isinstance(tran.postings[1], model.Posting)
        assert tran.postings[1].account == "posting2"

    def test_parse_dates(self):
        ctx = model.Context(conf.Config())
        inp = [
            "2021-07-14=2021-07-10 something",
            "    Expense:abc  10.2 EUR",
            "    Account:qwe",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)
        assert tran.date2 == date(2021, 7, 10)

    def test_parse_date_autoformat(self):
        ctx = model.Context(conf.Config())
        inp = ["2021/07/14 something", "    Account:qwe"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)

        inp = ["2021.07.14 something", "    Account:qwe"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)

        inp = ["21/07/14 something", "    Account:qwe"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)

        inp = ["21-07-14 something", "    Account:qwe"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)

        inp = ["21.07.14 something", "    Account:qwe"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date == date(2021, 7, 14)

    def test_parse_numbers(self):
        ctx = model.Context(conf.Config())
        inp = ["2021-07-14 something (123)  ; a transaction comment"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something (123)"
        assert tran.numbers == ["123"]

        inp = ["2021-07-14 something (123,321)# a transaction comment"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something (123,321)"
        assert tran.numbers == ["123", "321"]

        inp = ["2021-07-14 something (123,321)"]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.raw == inp[0]
        assert tran.message == "something (123,321)"
        assert tran.numbers == ["123", "321"]


class TestParsePeriodic:
    def test_parse_periodic_trans(self):
        ctx = model.Context(conf.Config())
        inp = [
            "~ monthly",
            "    Expense:abc  10.2 EUR",
            "    Account:qwe",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date is None
        assert tran.date2 is None
        assert tran.message == "monthly"
        assert tran.is_periodic

        ctx = model.Context(conf.Config())
        inp = [
            "~ monthly from 2023-04-15 to 2023-06-16",
            "    Expense:abc  10.2 EUR",
            "    Account:qwe",
        ]
        tran = parser.parse_transaction(ctx, list(enumerate(inp)))
        assert isinstance(tran, model.Transaction)
        assert tran.date is None
        assert tran.message == "monthly from 2023-04-15 to 2023-06-16"
        assert tran.is_periodic
