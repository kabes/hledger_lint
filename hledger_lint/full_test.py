# Copyright © 2024 Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.
# mypy: disable-error-code=no-untyped-def

""" """

import typing as ty

import pytest

from . import conf, main, model

_sample_ok_1 = """
account Aktywa:Aktywa bieżące:Gotówka
account Aktywa:Aktywa bieżące:ROR
account Przychód:Zwroty
account Wydatki:Artykuły spożywcze
account Przychód:Przychód z inwestycji
account equity:opening/closing balances
commodity 1.000,00 PLN
decimal-mark ,
payee aaaa
payee bbbb
payee ccccc

Y 2024

2024-01-01 opening balances
    Aktywa:Aktywa bieżące:Gotówka          1.234,56 PLN = 1.234,56 PLN
    equity:opening/closing balances

2024-01-04 * aaaa|bbbb (1010)
    Aktywa:Aktywa bieżące:ROR                   +100,01 PLN
    Przychód:Zwroty

2024-01-05 * bbbb|shopping
    Aktywa:Aktywa bieżące:Gotówka               -222,33 PLN
    Wydatki:Artykuły spożywcze

2024-01-08 * ccccc|aaa (1011)
    Aktywa:Aktywa bieżące:ROR                    +12,34 PLN = 1.346,91 PLN
    Przychód:Przychód z inwestycji               -12,34 PLN

commodity 100,000.00 EUR

2024-01-09 * ccccc|aaa (1012)
    Aktywa:Aktywa bieżące:ROR               10,00 PLN @ 1.23 EUR
    Przychód:Przychód z inwestycji         -12.30 EUR

"""


@pytest.mark.parametrize(
    "test_data",
    [_sample_ok_1],
)
def test_sample_ok(test_data):
    cfg = conf.Config()
    cfg.payees = ["opening balances", "closing balances"]
    cfg.validate_payee = True
    ctx = model.Context(cfg)
    res = list(main.process(ctx, test_data.split("\n")))
    assert res == []


_sample_nok_1 = """
account Aktywa:Aktywa bieżące:Gotówka
account Aktywa:Aktywa bieżące:ROR
account Przychód:Zwroty
account Wydatki:Artykuły spożywcze
account Przychód:Przychód z inwestycji
account equity:opening/closing balances
commodity 100,00 PLN
decimal-mark ,

Y 2024
payee aaaa
payee bbbb
payee ccccc

2024-01-03 opening balances
    Aktywa:Aktywa bieżące:Gotówka    1,234.56 PLN = 1.234.56 PLN  ; 17:W02
    equity:opening/closing balances

2024-01-01 * aaaa|bbbb (1010)  ; 21, E10
    Aktywa:Aktywa bieżące:ROR                   -100,01 PLN
    Przychód:Zwroty                             +100,01 PLN  ; 22, E04

2024-01-05 * bbbb|shopping
    Aktywa:Aktywa bieżące:Gotówka               -222,33 PLN
    Wydatki:Artykuły spo       ; 26; E02

2024-01-08 * ccccc|aaa (1011)  ; 28: E17
    Aktywa:Aktywa bieżące:ROR                    +12,34 PLN = 1346,91 PLN
    Przychód:Przychód z inwestycji               -122,34 PLN

commodity 100,000.00 EUR

2024-01-09 * ccccc|aaa (1012)   ; 34: E17
    Aktywa:Aktywa bieżące:ROR               10,00 PLN @@ 1.23 EUR
    Przychód:Przychód z inwestycji         -1,230.00 EUR

2024-01-09 * ccccc|aaa (1013)  ; 38: E27
    Przychód:Przychód z inwestycji         -130,00 EUR  ; 39:W02

2024-01-10 ! ccccc|aaa (1013)   ; 41:E09; W03

2024-01-11 * aaa  ; 43: E27
    Przychód:Przychód z inwestycji         0 EUR  ; 44:E01

2024-01-11 * aaa|  ; 46:E15
    Aktywa:Aktywa bieżące:ROR         +100,00 PLN
    Wydatki:Artykuły spożywcze         -100.00 EUR  ; 48:E05

2024-01-12=2024-02-01 |2312313  ; 50: E11, E14
    Aktywa:Aktywa bieżące:ROR         -100,00  ; 51: E06
    Wydatki:Artykuły spożywcze         +100.00 RRR  ; 52: E07
"""


def _extract_errors(
    errors: ty.Iterable[tuple[int, model.Error]],
) -> ty.Iterable[tuple[int, str]]:
    for line_no, err in errors:
        yield line_no, err.code


@pytest.mark.parametrize(
    "test_data",
    [_sample_nok_1],
)
def test_sample_nok(test_data):
    ctx = model.Context(conf.Config())
    ctx.conf.validate_payee = True
    res = sorted(_extract_errors(main.process(ctx, test_data.split("\n"))))

    assert res == [
        (16, "W04"),
        (17, "W02"),
        (20, "E10"),
        (22, "E04"),
        (26, "E02"),
        (28, "E17"),
        (34, "E17"),
        (38, "E27"),
        (39, "W02"),
        (41, "E09"),
        (41, "W03"),
        (43, "E27"),
        (43, "W04"),
        (44, "E01"),
        (46, "E15"),
        (46, "W04"),
        (48, "E05"),
        (50, "E11"),
        (50, "E14"),
        (51, "E06"),
        (52, "E07"),
    ]
