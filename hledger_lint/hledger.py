# Copyright © 2022-2023, Karol Będkowski <Karol Będkowski@kkomp>
#
# Distributed under terms of the GPLv3 license.

""" """

import string
import typing as ty
from contextlib import suppress
from enum import StrEnum
from functools import cache

__all__ = [
    "AccountType",
    "guess_account_type",
    "expand_type_shortcut",
    "is_amount_match_format",
]


class AccountType(StrEnum):
    UNKNOWN = "unknown"
    LIABILITY = "liability"
    EQUITY = "equity"
    REVENUE = "revenue"
    EXPENSE = "expense"
    ASSET = "asset"
    CASH = "cash"


_ACCOUNT_ROOT_TYPES = {
    "dept": AccountType.LIABILITY,
    "depts": AccountType.LIABILITY,
    "liability": AccountType.LIABILITY,
    "liabilities": AccountType.LIABILITY,
    "equity": AccountType.EQUITY,
    "income": AccountType.REVENUE,
    "incomes": AccountType.REVENUE,
    "revenue": AccountType.REVENUE,
    "revenues": AccountType.REVENUE,
    "expense": AccountType.EXPENSE,
    "expenses": AccountType.EXPENSE,
    # PL names
    "pasywa": AccountType.LIABILITY,
    "kapitał": AccountType.EQUITY,
    "handlowe": AccountType.EQUITY,
    "przychód": AccountType.REVENUE,
    "przychody": AccountType.REVENUE,
    "wydatek": AccountType.EXPENSE,
    "wydatki": AccountType.EXPENSE,
}

_ASSETS_ROOT = (
    "assets",
    "asserts",
    # pl names
    "aktywa",
)

_ASSETS_ACCOUNTS = (
    "investment",
    "receivable",
    ":a/r",
    ":fixed",
    # pl names
    "inwestycje",
    "należności",
)


@cache
def guess_account_type(account: str) -> AccountType | None:
    """Accounts type definition in hledger"""

    account = account.lower()
    account_root, *_ = account.partition(":")

    if account_root in _ASSETS_ROOT:
        for asac in _ASSETS_ACCOUNTS:
            if asac in account:
                return AccountType.ASSET

        return AccountType.CASH

    return _ACCOUNT_ROOT_TYPES.get(account_root)


_ACCOUNT_TYPES_MAP = {
    "a": AccountType.ASSET,
    "l": AccountType.LIABILITY,
    "e": AccountType.EQUITY,
    "r": AccountType.REVENUE,
    "x": AccountType.EXPENSE,
    "c": AccountType.CASH,
}


def expand_type_shortcut(account_type: str) -> AccountType | None:
    if at := _ACCOUNT_TYPES_MAP.get(account_type):
        return at

    with suppress(ValueError):
        return AccountType(account_type)

    return None


def _split_at_nonnumber(instr: str) -> tuple[str, str]:
    # for currency at the end
    for idx, c in enumerate(instr):
        if c not in "1234567890-+,.' ":
            return instr[:idx].strip(), instr[idx:].strip()

    return instr, ""


def _rsplit_at_nonnumber(instr: str) -> tuple[str, str]:
    # for currency at the beginning
    for idx, c in enumerate(reversed(instr)):
        if c not in "1234567890-+,.' ":
            i = len(instr) - idx
            return instr[:i].strip(), instr[i:].strip()

    return instr, ""


def _compare(inp: ty.Iterable[str], fmt: ty.Iterable[str]) -> bool:
    return all(
        (f == a or (f in string.digits and a in string.digits))
        for a, f in zip(inp, fmt, strict=False)
    )


def is_amount_match_format(
    amount: str, fmt: str, ignore: str = "", decimal_mark: str | None = None
) -> bool:
    # first currency
    if fmt[0] not in "10+-":
        # when currency is on beginning, first split format and amount and
        # compare currency; then compare rest
        fmt_cur, fmt_amount = _rsplit_at_nonnumber(fmt)
        amt_cur, amt_amount = _rsplit_at_nonnumber(amount)
    else:
        # currency at the end
        fmt_amount, fmt_cur = _split_at_nonnumber(fmt)
        amt_amount, amt_cur = _split_at_nonnumber(amount)

    if fmt_cur != amt_cur or not amt_amount:
        return False

    if ignore:
        amt_amount = amt_amount.replace(ignore, "")
        fmt_amount = fmt_amount.replace(ignore, "")

    # add missing decimal mark at the end if fmt contains it
    if fmt_amount[-1] in ".," and amt_amount[-1] != fmt_amount[-1]:
        amt_amount += fmt_amount[-1]

    # strip +/- characters from amount and fmt
    if amt_amount[0] in "-+":
        amt_amount = amt_amount[1:]

    if fmt_amount[0] in "-+":
        fmt_amount = fmt_amount[1:]

    # when no defined decimal_mark compare amount with format entirely
    if not decimal_mark:
        return _compare(reversed(amt_amount), reversed(fmt_amount))

    # decimal mark is given, so compare parts separately
    amt_amount_i, _, amt_amount_f = amt_amount.rpartition(decimal_mark)
    fmt_amount_i, _, fmt_amount_f = fmt_amount.rpartition(decimal_mark)

    # if frac part is shorted - add 0; if longer than format - fail
    if (diff := (len(fmt_amount_f) - len(amt_amount_f))) > 0:
        amt_amount_f += "0" * diff
    elif diff < 0:
        return False

    return _compare(amt_amount_f, fmt_amount_f) and _compare(
        reversed(amt_amount_i), reversed(fmt_amount_i)
    )
